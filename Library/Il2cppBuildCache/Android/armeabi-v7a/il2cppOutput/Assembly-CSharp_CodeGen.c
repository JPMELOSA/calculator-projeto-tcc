﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GUI_Demo::Start()
extern void GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462 (void);
// 0x00000002 System.Void GUI_Demo::OnGUI()
extern void GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7 (void);
// 0x00000003 System.Void GUI_Demo::DoMyWindow(System.Int32)
extern void GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7 (void);
// 0x00000004 System.Void GUI_Demo::.ctor()
extern void GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3 (void);
// 0x00000005 System.Void switch_scenes::Start()
extern void switch_scenes_Start_mA8D9A4D44D99D2B58CBBC518A780740D58721009 (void);
// 0x00000006 System.Void switch_scenes::Update()
extern void switch_scenes_Update_m875654AADB9D17BE5547EFD83A0F955058BF56AC (void);
// 0x00000007 System.Void switch_scenes::.ctor()
extern void switch_scenes__ctor_m989FDA83C25E947189EF9847328E722852C4CECD (void);
// 0x00000008 System.Void switch_scenes::<Start>b__1_0()
extern void switch_scenes_U3CStartU3Eb__1_0_mAFDD8E35E748180891E696AD071066A70FA41AFF (void);
// 0x00000009 UnityEngine.RectTransform MSJoystick::get_rectTransform()
extern void MSJoystick_get_rectTransform_m86059C73213015238D5DE0EB82CAE49E86341C27 (void);
// 0x0000000A System.Void MSJoystick::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void MSJoystick_OnBeginDrag_m8DC24E6BF83E8118D672B7942A14AD5402F68617 (void);
// 0x0000000B System.Void MSJoystick::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void MSJoystick_OnEndDrag_m8CFB0EEF70FE2F0135125C7B4FCBB980A8B110F1 (void);
// 0x0000000C System.Void MSJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void MSJoystick_OnDrag_mBB1DF2AE1C7D5446B6AC2913AA70B888DB534240 (void);
// 0x0000000D System.Void MSJoystick::OnDeselect()
extern void MSJoystick_OnDeselect_m7C6D1251E80BE6A633A999FEC54400D605CDB18E (void);
// 0x0000000E System.Void MSJoystick::LateUpdate()
extern void MSJoystick_LateUpdate_m2A1D2EC9EA4B3E1AD5D9B3B0132B93ABE211EBE9 (void);
// 0x0000000F System.Void MSJoystick::SetAxisMS(UnityEngine.Vector2)
extern void MSJoystick_SetAxisMS_m96E70CC7737FD0A992DA8915F6EA8C92970D0603 (void);
// 0x00000010 System.Void MSJoystick::UpdateJoystickGraphicMS()
extern void MSJoystick_UpdateJoystickGraphicMS_mC59AF89D40E7A8BF9D416E51CD84C9A5DF38FC0B (void);
// 0x00000011 System.Void MSJoystick::.ctor()
extern void MSJoystick__ctor_mBEC734443D847C8E216BA5D408ABE57BFD36EED2 (void);
// 0x00000012 System.Void MSJoystickController::Update()
extern void MSJoystickController_Update_mDDEBB4361EF8411673E7991FC64671E56ED672C4 (void);
// 0x00000013 System.Void MSJoystickController::.ctor()
extern void MSJoystickController__ctor_m373271A7733AB522E66BD52E838B77462B16C4E4 (void);
// 0x00000014 System.Void AudioManager::Awake()
extern void AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01 (void);
// 0x00000015 System.Void AudioManager::PlayMusic(System.String)
extern void AudioManager_PlayMusic_mB50D212EBE632BF1E95380A7F222B043A0FBFFF1 (void);
// 0x00000016 System.Void AudioManager::PlaySFX(System.String)
extern void AudioManager_PlaySFX_m13FCA19E8F5B2A013718669280D1AABFB4767E03 (void);
// 0x00000017 System.Void AudioManager::PlayAudioFromTheRocks(System.Boolean,System.String)
extern void AudioManager_PlayAudioFromTheRocks_m6689B17CB6ADAB172E096EED74865A5EC27EDFCD (void);
// 0x00000018 System.Void AudioManager::_EFX1Ajust(System.Single)
extern void AudioManager__EFX1Ajust_m3C460A48D6AD2DEC1BEB668AF6216BE8F9B40D64 (void);
// 0x00000019 System.Void AudioManager::_EFX2Ajust(System.Single)
extern void AudioManager__EFX2Ajust_mB71A9EE34551F678543829A5E17F04DC611BA1FA (void);
// 0x0000001A System.Void AudioManager::_MUSICAjust(System.Single)
extern void AudioManager__MUSICAjust_m030B0B836806AD125659B4F3AC21995DF6480140 (void);
// 0x0000001B System.Void AudioManager::SaveAudioSetup()
extern void AudioManager_SaveAudioSetup_m26BCA901ACDA60922DFB71CD7972C196673358C2 (void);
// 0x0000001C System.Void AudioManager::LoadAudioSetup()
extern void AudioManager_LoadAudioSetup_m0C530D84037829EC041ECA7EC1DCC0F5BDF9F60C (void);
// 0x0000001D System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x0000001E System.Void AumentaVelocidade::Awake()
extern void AumentaVelocidade_Awake_m40F7D7B352D7A020A8A8B82EADF9AC76913A2747 (void);
// 0x0000001F System.Void AumentaVelocidade::Start()
extern void AumentaVelocidade_Start_m6722AC7BA8ECC2700A17C350FD687BAEB5FB118F (void);
// 0x00000020 System.Void AumentaVelocidade::Update()
extern void AumentaVelocidade_Update_mF42566D568DEEB98FC4ACD71F81592DE93D33525 (void);
// 0x00000021 System.Void AumentaVelocidade::recebeVelocidade(System.Single)
extern void AumentaVelocidade_recebeVelocidade_m07F2F6EDF044AFBC7CC166574078A3D0A8FD1B41 (void);
// 0x00000022 System.Void AumentaVelocidade::.ctor()
extern void AumentaVelocidade__ctor_m615D73D75460DE5722CCAE05FFF8C18BE04417A2 (void);
// 0x00000023 System.Void Calculadora::Awake()
extern void Calculadora_Awake_m23D5ADD5972AC2B1E0C90A497BDE1D4445ECB76B (void);
// 0x00000024 System.Void Calculadora::operacao(System.Single,System.Single,System.Char)
extern void Calculadora_operacao_mBACB4F365965A6619DCFEB7D774375F68B151703 (void);
// 0x00000025 System.Void Calculadora::sortearPorta()
extern void Calculadora_sortearPorta_m436BCE65D81671B44F202B4726626FEE63CF6086 (void);
// 0x00000026 System.Void Calculadora::executaOperacao(System.Char)
extern void Calculadora_executaOperacao_mC0CB12F0B3DF3F729BF9083DDD6D37BF91A0E802 (void);
// 0x00000027 System.Void Calculadora::respawPortas()
extern void Calculadora_respawPortas_m896CA76CB113C76DB7F42A50971F174B69753BAA (void);
// 0x00000028 System.Void Calculadora::.ctor()
extern void Calculadora__ctor_mAEC7C0155367249D2C85E34479B73BA898F8263B (void);
// 0x00000029 System.Void Config::Start()
extern void Config_Start_m4C931C9FC5362D375490C765CE1096A19CBD6E61 (void);
// 0x0000002A System.Void Config::Update()
extern void Config_Update_m6A1B69535117A9999608EAC7B7A567448561EDBB (void);
// 0x0000002B System.Void Config::soundOnOff()
extern void Config_soundOnOff_mF16B48B5C86113E4149ECDF10FF2F828FB827DD8 (void);
// 0x0000002C System.Void Config::musicOnOff()
extern void Config_musicOnOff_mC5E5930726300BE1F3FFCAA24DAB06C3C4E8E0F4 (void);
// 0x0000002D System.Void Config::desligaSom(System.Boolean)
extern void Config_desligaSom_m4A7739BD5EE36C48B247820A9A2EB456DAF42715 (void);
// 0x0000002E System.Void Config::iniciaAudioMaster()
extern void Config_iniciaAudioMaster_mAA759E925018D03DFDCCC073A181471BBDF989E4 (void);
// 0x0000002F System.Void Config::VolumeMaster(System.Single)
extern void Config_VolumeMaster_m88A7B905D815FD3E07F69B36FDB0E52DC05F078A (void);
// 0x00000030 System.Void Config::.ctor()
extern void Config__ctor_m74DB672E7C9821A5A767CDBA9BFA3060710B4712 (void);
// 0x00000031 System.Void Configuration::Start()
extern void Configuration_Start_m8FE82130A1A210D422327BC611C53585B8C8D3F8 (void);
// 0x00000032 System.Void Configuration::.ctor()
extern void Configuration__ctor_mB313DBB5812D634A19CC8B53D9A873A709B64C96 (void);
// 0x00000033 System.Void MenuPrincipal::Start()
extern void MenuPrincipal_Start_mE4E6BC42308F78D60A85F4364869150336D4ADC1 (void);
// 0x00000034 System.Void MenuPrincipal::start()
extern void MenuPrincipal_start_mA2F96F922A5ADC5B3CB5B372A96DBEFAEFD3869B (void);
// 0x00000035 System.Void MenuPrincipal::aprender()
extern void MenuPrincipal_aprender_mAE6BE86BA7CBB863388C5DE4EF3551525F42E381 (void);
// 0x00000036 System.Void MenuPrincipal::ranking()
extern void MenuPrincipal_ranking_m1839D883FC13CE59541D3C9E1F1EB29D13CF51BB (void);
// 0x00000037 System.Void MenuPrincipal::ajuda()
extern void MenuPrincipal_ajuda_mED87B60E79FB17401F323D8492DA3437286C81C0 (void);
// 0x00000038 System.Void MenuPrincipal::sobre()
extern void MenuPrincipal_sobre_m5E81D753929703812661C962C0870AA410FE89F2 (void);
// 0x00000039 System.Void MenuPrincipal::config()
extern void MenuPrincipal_config_mB2936436B1A8A4E5A26390A1D3BF7600E65D4648 (void);
// 0x0000003A System.Void MenuPrincipal::returnMenu()
extern void MenuPrincipal_returnMenu_m9342D2C134C48F015E946656161AA49E17DED1FB (void);
// 0x0000003B System.Void MenuPrincipal::adicao()
extern void MenuPrincipal_adicao_mBD7671A5A8873FA1521890E76AB1FA38847F88DB (void);
// 0x0000003C System.Void MenuPrincipal::subtracao()
extern void MenuPrincipal_subtracao_m8B1890B99B664E1DA3ED6722831281282FB5D4A3 (void);
// 0x0000003D System.Void MenuPrincipal::multiplicacao()
extern void MenuPrincipal_multiplicacao_mB58F8CD8E1F4103B12499634DED9701CF9ECB6E5 (void);
// 0x0000003E System.Void MenuPrincipal::divisao()
extern void MenuPrincipal_divisao_m9F700F5DA18A661CBD2F6DE181A03B9BD428D9A5 (void);
// 0x0000003F System.Void MenuPrincipal::tabelaErros()
extern void MenuPrincipal_tabelaErros_m5CF7FA7A55BA531AA3CCC9756BCBC8FDE46C1872 (void);
// 0x00000040 System.Void MenuPrincipal::iniciaAudioMaster()
extern void MenuPrincipal_iniciaAudioMaster_mFBCE540A1C053BA95407E042417E3AD3B9811701 (void);
// 0x00000041 System.Void MenuPrincipal::.ctor()
extern void MenuPrincipal__ctor_mB9DE46999E38C9C587A287D85E6DD1977760C1C4 (void);
// 0x00000042 System.Void MenuSceneManager::Awake()
extern void MenuSceneManager_Awake_m9F9DBA447541637778C280B595F2E5E672C5AD86 (void);
// 0x00000043 System.Void MenuSceneManager::Start()
extern void MenuSceneManager_Start_m501507B088BBF8240CFD9F13A8D15E984E5AE275 (void);
// 0x00000044 System.Void MenuSceneManager::_Pause()
extern void MenuSceneManager__Pause_m872B5CEBB89106EF78210C9ABB3739F7CA99B360 (void);
// 0x00000045 System.Void MenuSceneManager::cover(System.Boolean)
extern void MenuSceneManager_cover_mDE1D247160985D39B9137BAAAC035C5D3648B634 (void);
// 0x00000046 System.Void MenuSceneManager::abrirButtons(System.Boolean)
extern void MenuSceneManager_abrirButtons_m0E230A9CB0D6E01ED2FE6BA8BEF04DDD8004D6FB (void);
// 0x00000047 System.Void MenuSceneManager::_adicao()
extern void MenuSceneManager__adicao_mDD21C1D8CC25B320D1341175D73F063DE48DDAC7 (void);
// 0x00000048 System.Void MenuSceneManager::_divisao()
extern void MenuSceneManager__divisao_mDD6441FB175EEC7195D96FBBDB2362570F0CD7ED (void);
// 0x00000049 System.Void MenuSceneManager::_multiplicacao()
extern void MenuSceneManager__multiplicacao_mA08DB4A718A1EF64EE4913B0E5C74228594275DE (void);
// 0x0000004A System.Void MenuSceneManager::_subtracao()
extern void MenuSceneManager__subtracao_m3C011C9E9ED9835B9EC950DC2AF7A1600A678BBD (void);
// 0x0000004B System.Void MenuSceneManager::_botaoIniciar()
extern void MenuSceneManager__botaoIniciar_m557D4552F98EF7C0AFCC599A5569860B42A7295C (void);
// 0x0000004C System.Void MenuSceneManager::_retornarMenuPrincipal()
extern void MenuSceneManager__retornarMenuPrincipal_m85EC7C551CF14D18A2C9039C31DB36C3F2334BE8 (void);
// 0x0000004D System.Collections.IEnumerator MenuSceneManager::desativaButtons()
extern void MenuSceneManager_desativaButtons_mCD3A70567B4CFFA7A73931EA11F5B72260DB172C (void);
// 0x0000004E System.Void MenuSceneManager::ButtonsAfterGameOver()
extern void MenuSceneManager_ButtonsAfterGameOver_m8393D2A7F5F4741F7FE6FD5438EB5A5C7DF83DD2 (void);
// 0x0000004F System.Void MenuSceneManager::.ctor()
extern void MenuSceneManager__ctor_m57DDDDBCD7568C95037EC0400634AFE698AB6682 (void);
// 0x00000050 System.Void MenuSceneManager/<desativaButtons>d__26::.ctor(System.Int32)
extern void U3CdesativaButtonsU3Ed__26__ctor_m6D0845F09C3DD6F2382E3E135F8BBFC0EEB214C0 (void);
// 0x00000051 System.Void MenuSceneManager/<desativaButtons>d__26::System.IDisposable.Dispose()
extern void U3CdesativaButtonsU3Ed__26_System_IDisposable_Dispose_mBB55A8D1B44EDF499076B78858309F23D61A9525 (void);
// 0x00000052 System.Boolean MenuSceneManager/<desativaButtons>d__26::MoveNext()
extern void U3CdesativaButtonsU3Ed__26_MoveNext_m6DBEB1AF65FD77E5D639AD985060815C48E0D948 (void);
// 0x00000053 System.Object MenuSceneManager/<desativaButtons>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdesativaButtonsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B45F07001BEE68A5A2D126B2608E5796864B35F (void);
// 0x00000054 System.Void MenuSceneManager/<desativaButtons>d__26::System.Collections.IEnumerator.Reset()
extern void U3CdesativaButtonsU3Ed__26_System_Collections_IEnumerator_Reset_m77A1EEA00659124B2624E69ACA62E31727B9B047 (void);
// 0x00000055 System.Object MenuSceneManager/<desativaButtons>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CdesativaButtonsU3Ed__26_System_Collections_IEnumerator_get_Current_m75FEC85CC6325A6F33756AF8C817732265CB7681 (void);
// 0x00000056 System.Void MovementButton::OnEnable()
extern void MovementButton_OnEnable_m1D8C0C1BC9B05093E70202B50D6E9F1E51D078E4 (void);
// 0x00000057 System.Void MovementButton::botaoEsquerdoClicado()
extern void MovementButton_botaoEsquerdoClicado_m64BE766EE61A55BB1A33EEC57B24721C3CBC8F63 (void);
// 0x00000058 System.Void MovementButton::botaoDireitoClicado()
extern void MovementButton_botaoDireitoClicado_mB1B1CF9646789CBA3682F2B25426E52DE0114F7C (void);
// 0x00000059 System.Void MovementButton::.ctor()
extern void MovementButton__ctor_m27C3B4408F63FBAD552853EFF68F4138B4AA5B1D (void);
// 0x0000005A System.Void PanelManager::OnEnable()
extern void PanelManager_OnEnable_m09547ED27D87F696B41F1FAF5EFC3826F3204E2D (void);
// 0x0000005B System.Void PanelManager::OpenPanel(UnityEngine.Animator)
extern void PanelManager_OpenPanel_m55C75D9E37F2CDE8B1670FBF979329E5D6D13E0F (void);
// 0x0000005C UnityEngine.GameObject PanelManager::FindFirstEnabledSelectable(UnityEngine.GameObject)
extern void PanelManager_FindFirstEnabledSelectable_m59C12281C9EAED20F4FBB9C820D4EAD2C2DA117B (void);
// 0x0000005D System.Void PanelManager::CloseCurrent()
extern void PanelManager_CloseCurrent_m20DE00E5BAE15FA1D4A052E555A3CC5C58A6A695 (void);
// 0x0000005E System.Collections.IEnumerator PanelManager::DisablePanelDeleyed(UnityEngine.Animator)
extern void PanelManager_DisablePanelDeleyed_mB120A21FD25F2168724F35B9FAD134487EF0507A (void);
// 0x0000005F System.Void PanelManager::SetSelected(UnityEngine.GameObject)
extern void PanelManager_SetSelected_m33A82F0C75C5439BE6C1FEE4B93BF4E3949FE882 (void);
// 0x00000060 System.Void PanelManager::.ctor()
extern void PanelManager__ctor_m945C0470B90313D67EDCED0D97AFADB78A190AFD (void);
// 0x00000061 System.Void PanelManager/<DisablePanelDeleyed>d__10::.ctor(System.Int32)
extern void U3CDisablePanelDeleyedU3Ed__10__ctor_m3595FCFEB5B1694DEAE36303E82C43199E309861 (void);
// 0x00000062 System.Void PanelManager/<DisablePanelDeleyed>d__10::System.IDisposable.Dispose()
extern void U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_m37D3EA493F4986A2E0A6263B72E96EF00126946B (void);
// 0x00000063 System.Boolean PanelManager/<DisablePanelDeleyed>d__10::MoveNext()
extern void U3CDisablePanelDeleyedU3Ed__10_MoveNext_m7BFBF535CE4EBEABFFB4B592D5BA81B83152B2AD (void);
// 0x00000064 System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82D3317E9507246F91AABBA087291CE3DFAD9B4C (void);
// 0x00000065 System.Void PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.Reset()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_mCCE2A629729D15A72499C8B3EBE2380829AA7291 (void);
// 0x00000066 System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m271F5CC65D4EAF362EEEB37323D10ABBE73D0554 (void);
// 0x00000067 System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x00000068 System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x00000069 System.Void Player::movimento()
extern void Player_movimento_mBE5B3B5A466F35ABF7FA41DBA8A0CA8D5D9ECEDC (void);
// 0x0000006A System.Void Player::OnTriggerEnter(UnityEngine.Collider)
extern void Player_OnTriggerEnter_mE273002981E5225ABE0C891B825A78BB9FE905E6 (void);
// 0x0000006B System.Void Player::OnTriggerExit(UnityEngine.Collider)
extern void Player_OnTriggerExit_mF514DAD7438D7BC1290C7340EAF245B55BFC33FD (void);
// 0x0000006C System.Void Player::playerDown()
extern void Player_playerDown_mF0D2067EF7836C4E39914F27DAD0B0EF421D72FE (void);
// 0x0000006D System.Void Player::AumentaVelocidadePorta()
extern void Player_AumentaVelocidadePorta_mCCED915CE6BF6FB6B32A91FCAE101A499E69B86C (void);
// 0x0000006E System.Void Player::Idle()
extern void Player_Idle_m10EF471CCAE716DABE927CE3BE7F0FF0B596F01C (void);
// 0x0000006F System.Void Player::left()
extern void Player_left_m2911D1AEE81C4AE5E28FC0C7FFB1710E8018CE28 (void);
// 0x00000070 System.Void Player::right()
extern void Player_right_m10034C7720491D3C099DBC65DC02C1D0B712C722 (void);
// 0x00000071 System.Void Player::Celebrate()
extern void Player_Celebrate_m45701906C9D71AE7B9A5F336EDD18BFBDF5D628B (void);
// 0x00000072 System.Collections.IEnumerator Player::stopCelebrating()
extern void Player_stopCelebrating_m957315807065DAA64622C1F8975C7077D1C065BF (void);
// 0x00000073 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000074 System.Void Player/<stopCelebrating>d__18::.ctor(System.Int32)
extern void U3CstopCelebratingU3Ed__18__ctor_m2B1F184CFA5F940DB0531F9D09E81BB158921BC2 (void);
// 0x00000075 System.Void Player/<stopCelebrating>d__18::System.IDisposable.Dispose()
extern void U3CstopCelebratingU3Ed__18_System_IDisposable_Dispose_m1D2A0522885D354FCBD633ABFB42E773D9FD3AFD (void);
// 0x00000076 System.Boolean Player/<stopCelebrating>d__18::MoveNext()
extern void U3CstopCelebratingU3Ed__18_MoveNext_m83B9F9EAD944D67A29F1621B8125407310159820 (void);
// 0x00000077 System.Object Player/<stopCelebrating>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CstopCelebratingU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E3684B231F429F7834F1A37BD9F129BC2700322 (void);
// 0x00000078 System.Void Player/<stopCelebrating>d__18::System.Collections.IEnumerator.Reset()
extern void U3CstopCelebratingU3Ed__18_System_Collections_IEnumerator_Reset_m5B109D0CB07C5A23D4DBBADFEE703C11C9F34CF7 (void);
// 0x00000079 System.Object Player/<stopCelebrating>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CstopCelebratingU3Ed__18_System_Collections_IEnumerator_get_Current_mA5E1A11D26CCF8E761C9FED52BD98BA4417A9E5E (void);
// 0x0000007A System.Void PortaSingle::Start()
extern void PortaSingle_Start_mBA8092B4EB8A11E845A6B147B87E0950AEEA217D (void);
// 0x0000007B System.Void PortaSingle::Update()
extern void PortaSingle_Update_mAC59D0AB0FB1A54E122E8B62906E13AB6194B335 (void);
// 0x0000007C System.Void PortaSingle::limite()
extern void PortaSingle_limite_mB09FE3AD863C042CC90A80BEE18BAEE1F3852FEA (void);
// 0x0000007D System.Void PortaSingle::gameOver()
extern void PortaSingle_gameOver_m9F6E2FEBC61577868C23FF7FF314A22C4B219B22 (void);
// 0x0000007E System.Void PortaSingle::playAudio()
extern void PortaSingle_playAudio_mDBAE5BE029D89823903C7F7E310511A969462B64 (void);
// 0x0000007F System.Void PortaSingle::mover()
extern void PortaSingle_mover_mD0CDB1A60DC88FA831D7819F3059FF09269F250A (void);
// 0x00000080 System.Void PortaSingle::.ctor()
extern void PortaSingle__ctor_mC83950417ED214EFD124C8F04F15E3EA8E7DDFAA (void);
// 0x00000081 System.Void Record::Awake()
extern void Record_Awake_m62E5E5F979F3177ED0BA78440FC11D9DB80F4228 (void);
// 0x00000082 System.Void Record::Start()
extern void Record_Start_mDB280E1F77A30CEF4573EFFC04837C4839F3732C (void);
// 0x00000083 System.Void Record::atualizaRecords()
extern void Record_atualizaRecords_m65BAE938E2C8896371730443E4AB21373A654436 (void);
// 0x00000084 System.Void Record::receberRecordsGravados()
extern void Record_receberRecordsGravados_m6A28CC19F5E2F17C5979500498165016DB2A30E1 (void);
// 0x00000085 System.Void Record::.ctor()
extern void Record__ctor_mA1FAB02A3F3CE8F3B3948E834F06AC4DC86F90B4 (void);
// 0x00000086 System.Void RespawManager::Awake()
extern void RespawManager_Awake_mB0515F0438AE05921BA89C08877CE8D0638F7D4A (void);
// 0x00000087 System.Void RespawManager::Update()
extern void RespawManager_Update_m23FC8F81B42DFF8E52FC830E0ADF306FD3A4045A (void);
// 0x00000088 System.Void RespawManager::InstaciarPortas()
extern void RespawManager_InstaciarPortas_mC5101431BF494C97439262072234B28DF6E80E82 (void);
// 0x00000089 System.Void RespawManager::inicio()
extern void RespawManager_inicio_m86AB1BCBED76A5594C4F0E01B63669994708CDAF (void);
// 0x0000008A System.Void RespawManager::colocaOperacao()
extern void RespawManager_colocaOperacao_mCAB436219BC9022C5267F5D35768F130A4D510B2 (void);
// 0x0000008B System.Void RespawManager::SetFireworks()
extern void RespawManager_SetFireworks_mA481107AD6E9ED70C1AD5B130C0CBA859931F043 (void);
// 0x0000008C System.Collections.IEnumerator RespawManager::DestroyFireworksClone(UnityEngine.GameObject,UnityEngine.GameObject,UnityEngine.GameObject)
extern void RespawManager_DestroyFireworksClone_m4F661F09D8C0CD07200917DD84BE408F5E802A45 (void);
// 0x0000008D System.Void RespawManager::.ctor()
extern void RespawManager__ctor_m7EBB97E5DD07F7D003DA64C2865249A82DCD42D5 (void);
// 0x0000008E System.Void RespawManager/<DestroyFireworksClone>d__13::.ctor(System.Int32)
extern void U3CDestroyFireworksCloneU3Ed__13__ctor_m6EA62449DB1045BD90493D1343B61DB9E1491771 (void);
// 0x0000008F System.Void RespawManager/<DestroyFireworksClone>d__13::System.IDisposable.Dispose()
extern void U3CDestroyFireworksCloneU3Ed__13_System_IDisposable_Dispose_m76B65423FDDD8F437E158D099BD31C4FB3287153 (void);
// 0x00000090 System.Boolean RespawManager/<DestroyFireworksClone>d__13::MoveNext()
extern void U3CDestroyFireworksCloneU3Ed__13_MoveNext_m05339AB817598C6E3DF82C01C70117AA257982AE (void);
// 0x00000091 System.Object RespawManager/<DestroyFireworksClone>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDestroyFireworksCloneU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6204070EE7BDBBA40C06BB1E8CB25E58F0862EC (void);
// 0x00000092 System.Void RespawManager/<DestroyFireworksClone>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDestroyFireworksCloneU3Ed__13_System_Collections_IEnumerator_Reset_m1AFCD57B6FF2F507AD153F5889D9F145BDCF0C48 (void);
// 0x00000093 System.Object RespawManager/<DestroyFireworksClone>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDestroyFireworksCloneU3Ed__13_System_Collections_IEnumerator_get_Current_m8F01E4889F4FCA7499F0D71B77EAF5C9A5805EFC (void);
// 0x00000094 System.Void SliderBarsConfiguration::Start()
extern void SliderBarsConfiguration_Start_mDACC334594F936D4C8C4B5470EE3714E5CA03968 (void);
// 0x00000095 System.Void SliderBarsConfiguration::_EFX1Ajust(System.Single)
extern void SliderBarsConfiguration__EFX1Ajust_m02A8C6BCC50506E8576A169A8E16FE34CE243DAF (void);
// 0x00000096 System.Void SliderBarsConfiguration::_EFX2Ajust(System.Single)
extern void SliderBarsConfiguration__EFX2Ajust_m5C9AE932486F80205DB60BEEE5F887C25A3F5A70 (void);
// 0x00000097 System.Void SliderBarsConfiguration::_MUSICAjust(System.Single)
extern void SliderBarsConfiguration__MUSICAjust_m5D8979F66BFC484A8C9DA6DE279DD1815C468DFC (void);
// 0x00000098 System.Void SliderBarsConfiguration::.ctor()
extern void SliderBarsConfiguration__ctor_m87E00FD51DA636E7FE0166756FD79C37DF085EE2 (void);
// 0x00000099 System.Void TabeladeErros::Awake()
extern void TabeladeErros_Awake_m8E9DB5DF4E7AB2A74FA6E86C66F781318236628E (void);
// 0x0000009A System.Void TabeladeErros::Start()
extern void TabeladeErros_Start_m9D2CE08099407C5F65A682647EED8DA68B47D7A6 (void);
// 0x0000009B System.Void TabeladeErros::gravaErro(System.Single,System.Single,System.Char)
extern void TabeladeErros_gravaErro_mC2641669F81EC22C2E30BE1C6A6A36FA8F28B440 (void);
// 0x0000009C System.Void TabeladeErros::atualizaTabela()
extern void TabeladeErros_atualizaTabela_mA600F3CA7EEC17A5CE082A8C1D2BC56B7441ADCF (void);
// 0x0000009D System.Void TabeladeErros::qntMulti(System.Char)
extern void TabeladeErros_qntMulti_m2FF7DC65727C37FCB43D30BEE91FF925365E4222 (void);
// 0x0000009E System.Void TabeladeErros::atualizaNumeroVezes()
extern void TabeladeErros_atualizaNumeroVezes_m5214D1A50122E6113E91A9CF8893F5156BFA53B5 (void);
// 0x0000009F System.Void TabeladeErros::.ctor()
extern void TabeladeErros__ctor_m94E0CDE2832716CE7C5739E1B8F7BF7F81695D11 (void);
// 0x000000A0 System.Void TransitionSceneManager::OnEnable()
extern void TransitionSceneManager_OnEnable_mB8383EDD9E2CA55B8132288ED9446BC5C0190CEB (void);
// 0x000000A1 System.Void TransitionSceneManager::Start()
extern void TransitionSceneManager_Start_m7F56CD1005EBA5D601166C0940175F9E83A34209 (void);
// 0x000000A2 System.Void TransitionSceneManager::ChangeScene(System.String,System.Single)
extern void TransitionSceneManager_ChangeScene_mB4CE23BA6C984733AF0938F5350EC68CDA7FD092 (void);
// 0x000000A3 System.Collections.IEnumerator TransitionSceneManager::Countdown(System.String,System.Single)
extern void TransitionSceneManager_Countdown_m8CB57B62D076BACDF24FCF1C0AB5AC192009C878 (void);
// 0x000000A4 System.Collections.IEnumerator TransitionSceneManager::GoMainMenu()
extern void TransitionSceneManager_GoMainMenu_mBBDC80B928BD3B5ECF93897045F2F64303D92654 (void);
// 0x000000A5 System.Void TransitionSceneManager::.ctor()
extern void TransitionSceneManager__ctor_m119EB7FAE132C33115802D130B4A261B4F778FB4 (void);
// 0x000000A6 System.Void TransitionSceneManager/<Countdown>d__7::.ctor(System.Int32)
extern void U3CCountdownU3Ed__7__ctor_mEF6C97BDAA7E1AE8D8C4890CB75A3C4BBF147EE2 (void);
// 0x000000A7 System.Void TransitionSceneManager/<Countdown>d__7::System.IDisposable.Dispose()
extern void U3CCountdownU3Ed__7_System_IDisposable_Dispose_m5A3E5BD204C52BEF7DEBD92BA3C41FF148B6F732 (void);
// 0x000000A8 System.Boolean TransitionSceneManager/<Countdown>d__7::MoveNext()
extern void U3CCountdownU3Ed__7_MoveNext_mEF4C27693E5B14C6B2EE2B43E6CDD014001C2E99 (void);
// 0x000000A9 System.Object TransitionSceneManager/<Countdown>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountdownU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m665A3EC933D039B9EA00E33895894CE8FBEE3E48 (void);
// 0x000000AA System.Void TransitionSceneManager/<Countdown>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCountdownU3Ed__7_System_Collections_IEnumerator_Reset_m68C5B82123EEA040131BE0F665096885E9B05E15 (void);
// 0x000000AB System.Object TransitionSceneManager/<Countdown>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCountdownU3Ed__7_System_Collections_IEnumerator_get_Current_m171E67D50838CCF5EEC358E14BAB0C97BE86C679 (void);
// 0x000000AC System.Void TransitionSceneManager/<GoMainMenu>d__8::.ctor(System.Int32)
extern void U3CGoMainMenuU3Ed__8__ctor_m2DA2014F66926CB509AA23D9E929177BA14F0DFF (void);
// 0x000000AD System.Void TransitionSceneManager/<GoMainMenu>d__8::System.IDisposable.Dispose()
extern void U3CGoMainMenuU3Ed__8_System_IDisposable_Dispose_mB8E4FDF2F51C30F221B1E9956C47E449599321DA (void);
// 0x000000AE System.Boolean TransitionSceneManager/<GoMainMenu>d__8::MoveNext()
extern void U3CGoMainMenuU3Ed__8_MoveNext_m022F3B59B165FB7F417FC0ECB5EE439BA36ABC06 (void);
// 0x000000AF System.Object TransitionSceneManager/<GoMainMenu>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGoMainMenuU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FCA6EC72AD4115E4B1DBC25A2E3AB1110319C8 (void);
// 0x000000B0 System.Void TransitionSceneManager/<GoMainMenu>d__8::System.Collections.IEnumerator.Reset()
extern void U3CGoMainMenuU3Ed__8_System_Collections_IEnumerator_Reset_m06E399196E0E93903DF007223CAE53153045715B (void);
// 0x000000B1 System.Object TransitionSceneManager/<GoMainMenu>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CGoMainMenuU3Ed__8_System_Collections_IEnumerator_get_Current_m2F461B1994A9B36D30218553B2F1BC370A5E931D (void);
// 0x000000B2 System.Void UIManager::Awake()
extern void UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30 (void);
// 0x000000B3 System.Void UIManager::Start()
extern void UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E (void);
// 0x000000B4 System.Void UIManager::mudaCor(System.Char)
extern void UIManager_mudaCor_m392E5B0AB40F79F245487AABEECF59F0FD2B0336 (void);
// 0x000000B5 System.Void UIManager::limpaPainel()
extern void UIManager_limpaPainel_mD45892622E19C92309518A11EE110BE5B6E98EB5 (void);
// 0x000000B6 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x000000B7 System.Void BtnAdicao::Start()
extern void BtnAdicao_Start_m1F800BA9C2CAB4A5C859399A8976FD477817A2EA (void);
// 0x000000B8 System.Void BtnAdicao::Update()
extern void BtnAdicao_Update_m04096539BF51ED67D00A4E623A09B01CD39CFC84 (void);
// 0x000000B9 System.Void BtnAdicao::adicao()
extern void BtnAdicao_adicao_m28370DAD697E0B171111870714E30A02ABAFEB5D (void);
// 0x000000BA System.Void BtnAdicao::.ctor()
extern void BtnAdicao__ctor_mEB2F14C5CF5EC08515CBB149FBD3A478434AF70B (void);
// 0x000000BB System.Void BtnDivisao::Start()
extern void BtnDivisao_Start_m2A9AA939817E9974D54AEFD7519E732D88D7BE4E (void);
// 0x000000BC System.Void BtnDivisao::Update()
extern void BtnDivisao_Update_m6C45D7BB89E3E3C17A264A386EAAB1B4E0CED7C0 (void);
// 0x000000BD System.Void BtnDivisao::divisao()
extern void BtnDivisao_divisao_m20B1663F66531CBAA47199D6613C12BC5825E88D (void);
// 0x000000BE System.Void BtnDivisao::.ctor()
extern void BtnDivisao__ctor_m0C12493AC464B86BACBE0CC3E87654058C553981 (void);
// 0x000000BF System.Void BtnIniciar::Start()
extern void BtnIniciar_Start_m2864945962A5A944AA96177EF05FAAE9E5063D32 (void);
// 0x000000C0 System.Void BtnIniciar::Update()
extern void BtnIniciar_Update_m92846AB4358845B9A3FC83D0E566C2C5B6E319D1 (void);
// 0x000000C1 System.Void BtnIniciar::botaoIniciar()
extern void BtnIniciar_botaoIniciar_m907950A94978A88005723623C5AC47F78BFD6DB1 (void);
// 0x000000C2 System.Void BtnIniciar::.ctor()
extern void BtnIniciar__ctor_mCCA75A8220C237DC37B161BB23E5171198DF2A54 (void);
// 0x000000C3 System.Void BtnMultiplicacao::Start()
extern void BtnMultiplicacao_Start_mC3FC3B13AE4D50ABB7C43C3B8B2F708139372A42 (void);
// 0x000000C4 System.Void BtnMultiplicacao::Update()
extern void BtnMultiplicacao_Update_m68FE120EA46DB373448550F8BA533FDC479F6637 (void);
// 0x000000C5 System.Void BtnMultiplicacao::multiplicacao()
extern void BtnMultiplicacao_multiplicacao_mA9A2003AD0EDE113145476CCDF8F5A0F81FC1C5A (void);
// 0x000000C6 System.Void BtnMultiplicacao::.ctor()
extern void BtnMultiplicacao__ctor_mEE09D46762BD61B11673EF6722B6B05F93937003 (void);
// 0x000000C7 System.Void BtnSubtracao::Start()
extern void BtnSubtracao_Start_m224B196706CEA5B0EB3D99875097D094FE20632E (void);
// 0x000000C8 System.Void BtnSubtracao::Update()
extern void BtnSubtracao_Update_m267F104A1A04BBD3E2E65847EA5B5F40354A4683 (void);
// 0x000000C9 System.Void BtnSubtracao::subtracao()
extern void BtnSubtracao_subtracao_m8909D43314C8559E2911490196D631A54F1F8CCA (void);
// 0x000000CA System.Void BtnSubtracao::.ctor()
extern void BtnSubtracao__ctor_m608907D9891883BE42E4FC0FDA94572B6A6C031A (void);
// 0x000000CB System.Void Grito::Start()
extern void Grito_Start_m9E58F564659F310C38F0889A1E80BBB767F3D035 (void);
// 0x000000CC System.Void Grito::Update()
extern void Grito_Update_mBAAC1010A9069DDA82063A1829DAE00E99A5E117 (void);
// 0x000000CD System.Void Grito::OnTriggerEnter(UnityEngine.Collider)
extern void Grito_OnTriggerEnter_m67449F0CE21025681D703E7722ADCD4F4E1BACE3 (void);
// 0x000000CE System.Void Grito::.ctor()
extern void Grito__ctor_mB8BCD4716FF58CF0C74291C0A5982B6685E32019 (void);
// 0x000000CF System.Void MovementManager::OnEnable()
extern void MovementManager_OnEnable_m560AF40B2966E61EF969CB9AF6136D960F37C856 (void);
// 0x000000D0 System.Void MovementManager::Start()
extern void MovementManager_Start_m5E101C7CB4F977AE843D5B15FAFA810A0F54EAF1 (void);
// 0x000000D1 System.Void MovementManager::Update()
extern void MovementManager_Update_mC3987BFB04EFAB5132C15588F96118333F36E2B5 (void);
// 0x000000D2 System.Void MovementManager::receiveReferrals()
extern void MovementManager_receiveReferrals_mCD5C7593D184AE70CCA9BDB177D6B4873F34355A (void);
// 0x000000D3 System.Void MovementManager::_MovLeft()
extern void MovementManager__MovLeft_m6EB43AAEFDFD99B462AA0BB598CE3FB78295330D (void);
// 0x000000D4 System.Void MovementManager::_MovRight()
extern void MovementManager__MovRight_m68040F21387EEC52B630A2A7E88C99665E0980F0 (void);
// 0x000000D5 System.Void MovementManager::Idle()
extern void MovementManager_Idle_mA77605339804F73BE47A82769DD83F6F1C4F2174 (void);
// 0x000000D6 System.Void MovementManager::left()
extern void MovementManager_left_m3840AE96E57E7FFE6E88983143A13632C7F1231C (void);
// 0x000000D7 System.Void MovementManager::right()
extern void MovementManager_right_m3738BD4B9671ED45656FE50BEF8B7A31DB16F481 (void);
// 0x000000D8 System.Void MovementManager::.ctor()
extern void MovementManager__ctor_mC9B41C1DB50F23ED5238525C50B434B735E442CF (void);
// 0x000000D9 System.Collections.IEnumerator PlayerLerper::Start()
extern void PlayerLerper_Start_mE9FC9BB378D367D0B28992858B0B1A0C9CEF4AD9 (void);
// 0x000000DA System.Void PlayerLerper::Update()
extern void PlayerLerper_Update_m960EAEC5D41A7D386A62FA732EBF3F43C1C1CB9F (void);
// 0x000000DB System.Collections.IEnumerator PlayerLerper::RepeatLerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void PlayerLerper_RepeatLerp_mAF6F7C368D07CC5BFF03409BFD042E1F1E5BC18E (void);
// 0x000000DC System.Void PlayerLerper::.ctor()
extern void PlayerLerper__ctor_m4643F8FAB2DE08280350185D49ADEAB265F076C8 (void);
// 0x000000DD System.Void PlayerLerper/<Start>d__6::.ctor(System.Int32)
extern void U3CStartU3Ed__6__ctor_mC70C71B465A4ADB0257AC9D2EEDE9AE9B3746ACA (void);
// 0x000000DE System.Void PlayerLerper/<Start>d__6::System.IDisposable.Dispose()
extern void U3CStartU3Ed__6_System_IDisposable_Dispose_m4C41C16D10F9FA73F4FD339B55A51FC5ABABBDDE (void);
// 0x000000DF System.Boolean PlayerLerper/<Start>d__6::MoveNext()
extern void U3CStartU3Ed__6_MoveNext_m16442D90D77CA0218E9B2E4013D022231C8854E3 (void);
// 0x000000E0 System.Object PlayerLerper/<Start>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6240CA3CE7684B9EA3751110F8F78DEE445EBC1 (void);
// 0x000000E1 System.Void PlayerLerper/<Start>d__6::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m3E0672E0EA91A827DC3278894F085570A776AECA (void);
// 0x000000E2 System.Object PlayerLerper/<Start>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m04CC86276A34D99E92731B5927E09A25F40464E7 (void);
// 0x000000E3 System.Void PlayerLerper/<RepeatLerp>d__8::.ctor(System.Int32)
extern void U3CRepeatLerpU3Ed__8__ctor_m3C1CD7B493CC82965051D16CB07D77FB35BBC52A (void);
// 0x000000E4 System.Void PlayerLerper/<RepeatLerp>d__8::System.IDisposable.Dispose()
extern void U3CRepeatLerpU3Ed__8_System_IDisposable_Dispose_m87FD269733B3F423CB4EDBE4C55DB51FDBABA80B (void);
// 0x000000E5 System.Boolean PlayerLerper/<RepeatLerp>d__8::MoveNext()
extern void U3CRepeatLerpU3Ed__8_MoveNext_mA716C3E872A1773F5485193FD771D31F8232D600 (void);
// 0x000000E6 System.Object PlayerLerper/<RepeatLerp>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatLerpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC16C1751F17D4FCB8E57A54DA4A08AB2C73BA011 (void);
// 0x000000E7 System.Void PlayerLerper/<RepeatLerp>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRepeatLerpU3Ed__8_System_Collections_IEnumerator_Reset_mBEF8E1F84572D51241D6A0145F5416CCC8411DFC (void);
// 0x000000E8 System.Object PlayerLerper/<RepeatLerp>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatLerpU3Ed__8_System_Collections_IEnumerator_get_Current_mC0544794445FD91614B7905B78931CF027A0A466 (void);
// 0x000000E9 System.Void ReturnMenu::Start()
extern void ReturnMenu_Start_mF0D308EE802E0A442688096F2B76DD03DE69B267 (void);
// 0x000000EA System.Void ReturnMenu::Update()
extern void ReturnMenu_Update_mB02E84594338DF6558AE453FBDE13606B1BC4642 (void);
// 0x000000EB System.Void ReturnMenu::retornar()
extern void ReturnMenu_retornar_m2C1C3D277930CDF5CF6DF30411E809EE0E26AC07 (void);
// 0x000000EC System.Void ReturnMenu::.ctor()
extern void ReturnMenu__ctor_mAB307DE82BD6BB0F85B38C071444A343C1F737D1 (void);
// 0x000000ED System.Void Roda::Start()
extern void Roda_Start_mDB4CD9A767A5ED3AFEC68E3760E580183E3CBAEC (void);
// 0x000000EE System.Void Roda::Update()
extern void Roda_Update_m0899ED8351B5513B8B2C4BDDD3640C9EEC71C3AB (void);
// 0x000000EF System.Void Roda::.ctor()
extern void Roda__ctor_mA06EABC969CDBAC2D00D84CAA384C4F10450A968 (void);
// 0x000000F0 System.Void VirtualJoystick::Start()
extern void VirtualJoystick_Start_m54B90FE037FDFD13244077A91EEAFA888ED6BBC0 (void);
// 0x000000F1 System.Void VirtualJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void VirtualJoystick_OnDrag_m3DC97C2455C85EA1AAAEE893BF6107590F32223E (void);
// 0x000000F2 System.Void VirtualJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VirtualJoystick_OnPointerDown_m405614EFCBD672DDE5F07289E5ABDFEEC13B86AB (void);
// 0x000000F3 System.Void VirtualJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VirtualJoystick_OnPointerUp_mFE9F180134AEC17FFEF8AC503B6880D6B1CCA79E (void);
// 0x000000F4 System.Void VirtualJoystick::Update()
extern void VirtualJoystick_Update_mE5916F6E2B087A0AA5747900E675590F4F9D877F (void);
// 0x000000F5 System.Void VirtualJoystick::.ctor()
extern void VirtualJoystick__ctor_m13CD0570C7630D04EAB8081592F05590DCF5F2D5 (void);
// 0x000000F6 System.Void CameraLogic::Start()
extern void CameraLogic_Start_mA116341F9A1BB67B349AC114E3F0D34623E611C9 (void);
// 0x000000F7 System.Void CameraLogic::SwitchTarget(System.Int32)
extern void CameraLogic_SwitchTarget_m25361D85881175425D3052C5CF29FEE0E6898BC4 (void);
// 0x000000F8 System.Void CameraLogic::NextTarget()
extern void CameraLogic_NextTarget_mFA284EE569009D52295112739EA96E85C9D3C0DC (void);
// 0x000000F9 System.Void CameraLogic::PreviousTarget()
extern void CameraLogic_PreviousTarget_m288C0528639524275FE571D019ABE4ED86240C10 (void);
// 0x000000FA System.Void CameraLogic::Update()
extern void CameraLogic_Update_mAAD604ABB9F03AEA994EAC4FE606C8CB2A7E40D5 (void);
// 0x000000FB System.Void CameraLogic::LateUpdate()
extern void CameraLogic_LateUpdate_m17F5CF81B28F1FC71D73BFBE93161BDD37D77C4E (void);
// 0x000000FC System.Void CameraLogic::.ctor()
extern void CameraLogic__ctor_m8ED9E6EEAEB3ED54C9277289D85DBB4815A34A66 (void);
// 0x000000FD System.Void Demo::Start()
extern void Demo_Start_mA96D71BE495F276B026F0223EAAF21A1B4C626E8 (void);
// 0x000000FE System.Void Demo::Update()
extern void Demo_Update_mA20CD8F8E6C14018B5051CFC4624F1559E45BA40 (void);
// 0x000000FF System.Void Demo::OnGUI()
extern void Demo_OnGUI_m43E8559CBC9366A8702DE5BE2066926C57825717 (void);
// 0x00000100 System.Void Demo::.ctor()
extern void Demo__ctor_mA1BCCF87BB86D1772EABA9E89B452959AA579645 (void);
// 0x00000101 System.Void SimpleCharacterControl::OnCollisionEnter(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionEnter_mDD3D6DA9717BD197BBB64898AD5FCA816187240A (void);
// 0x00000102 System.Void SimpleCharacterControl::OnCollisionStay(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionStay_m3AEEB2C953BAB6C720598C0868A7976EC92F3355 (void);
// 0x00000103 System.Void SimpleCharacterControl::OnCollisionExit(UnityEngine.Collision)
extern void SimpleCharacterControl_OnCollisionExit_m806BAB2C01B7CF0B0581CA58736B1342E4CB6C00 (void);
// 0x00000104 System.Void SimpleCharacterControl::Update()
extern void SimpleCharacterControl_Update_m5681DBDA0F1B7A9546A62F43F557F93A1E1E48F7 (void);
// 0x00000105 System.Void SimpleCharacterControl::TankUpdate()
extern void SimpleCharacterControl_TankUpdate_m963FB4DCA9FC24A94405C841CD2A2D8D62693DD0 (void);
// 0x00000106 System.Void SimpleCharacterControl::DirectUpdate()
extern void SimpleCharacterControl_DirectUpdate_m33DCC602CE9F0EF409D4D2ECB4CBC18B3DFA16AE (void);
// 0x00000107 System.Void SimpleCharacterControl::JumpingAndLanding()
extern void SimpleCharacterControl_JumpingAndLanding_mE87AB9D0B87B5B13CA45EE8F8C250C8B89E416E8 (void);
// 0x00000108 System.Void SimpleCharacterControl::.ctor()
extern void SimpleCharacterControl__ctor_m859C68B295A4AF78ECC97E2AEE6FB88A661F833C (void);
// 0x00000109 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000010A System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000010B System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000010C System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000010D System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x0000010E System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x0000010F System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000110 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000111 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000112 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000113 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000114 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000115 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000116 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000117 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000118 System.Void playerScript::Start()
extern void playerScript_Start_m861652F32C1A038ECD57E6A298787E67D05979B2 (void);
// 0x00000119 System.Void playerScript::Update()
extern void playerScript_Update_m8B08A7D12C5CF8DD1219EE6BD4A1323DC9CE0DBE (void);
// 0x0000011A System.Void playerScript::movimento()
extern void playerScript_movimento_mB3586E846B1D16E993056CFB0D4C525D4ACDE616 (void);
// 0x0000011B System.Void playerScript::verifica_movimentacao()
extern void playerScript_verifica_movimentacao_mA82D4D60C971867B78A7484F366FFB98EFAE68ED (void);
// 0x0000011C System.Void playerScript::checkSwipe()
extern void playerScript_checkSwipe_mFFA33849CB5E59618B2F9A9BF397F4207F76D5CA (void);
// 0x0000011D System.Single playerScript::verticalMove()
extern void playerScript_verticalMove_mC6BF591F9A817E1AE3B086E112840DCC0116BAA4 (void);
// 0x0000011E System.Single playerScript::horizontalValMove()
extern void playerScript_horizontalValMove_m805E05D3A65E191125B18E04F5A84D0CCCAA16AC (void);
// 0x0000011F System.Void playerScript::resetaDirecao()
extern void playerScript_resetaDirecao_m05864B552C4CE94CCA042051A14E613B137DE3EE (void);
// 0x00000120 System.Void playerScript::.ctor()
extern void playerScript__ctor_m29F0FA3290E1B828026CA6BABF42D31B8CBBEE83 (void);
// 0x00000121 System.Void playerpc::Start()
extern void playerpc_Start_mBBABD64DAB510F24E61F30F97FC5617A33247136 (void);
// 0x00000122 System.Void playerpc::Update()
extern void playerpc_Update_m07A6BB7A5D1E71510CE7B0DDF8094755441C5F0F (void);
// 0x00000123 System.Void playerpc::movimentacao()
extern void playerpc_movimentacao_mFDA175C4DC9464C3B72CD12EC24AFB2E4B10DFF0 (void);
// 0x00000124 System.Void playerpc::verifica_movimentacao()
extern void playerpc_verifica_movimentacao_m7078ADA883738F03B97E69C0D744ED8575284BAC (void);
// 0x00000125 System.Void playerpc::resetaDirecao()
extern void playerpc_resetaDirecao_mC1BEB391B038FB4AC396F1FF5D6421BA3BC40C2D (void);
// 0x00000126 System.Void playerpc::.ctor()
extern void playerpc__ctor_m896802B1E7BA521454BD439E86AB564A5B09212B (void);
// 0x00000127 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x00000128 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000129 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000012A System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x0000012B TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000012C System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x0000012D TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x0000012E System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x0000012F TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000130 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000131 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000132 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000133 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000134 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000135 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000136 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000137 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x00000138 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000139 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000013A System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000013B System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000013C System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000013D System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x0000013E System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x0000013F System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000140 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000141 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000142 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000143 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000144 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000145 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000146 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000147 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000148 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x00000149 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000014A System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000014B System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000014C System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000014D System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000014E System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x0000014F System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000150 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000151 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000152 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000153 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000154 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x00000155 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x00000156 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x00000157 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000158 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x00000159 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000015A System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x0000015B System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x0000015C System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x0000015D System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x0000015E System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x0000015F System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000160 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000161 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000162 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000163 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000164 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x00000165 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x00000166 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x00000167 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000168 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000169 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000016A System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x0000016B System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x0000016C System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x0000016D System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x0000016E System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x0000016F System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000170 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000171 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000172 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000173 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000174 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000175 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x00000176 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x00000177 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000178 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x00000179 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000017A System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x0000017B System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x0000017C System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x0000017D System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x0000017E System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x0000017F System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000180 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000181 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000182 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000183 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000184 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000185 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000186 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000187 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000188 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x00000189 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000018A System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x0000018B System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x0000018C System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x0000018D System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x0000018E System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x0000018F System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000190 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000191 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x00000192 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x00000193 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x00000194 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x00000195 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000196 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x00000197 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x00000198 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x00000199 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x0000019A System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x0000019B System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x0000019C System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x0000019D System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x0000019E System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x0000019F System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000001A0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000001A1 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000001A2 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000001A3 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000001A4 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000001A5 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000001A6 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000001A7 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000001A8 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000001A9 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000001AA System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000001AB System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000001AC System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000001AD System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000001AE System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000001AF System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000001B0 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000001B1 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000001B2 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000001B3 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000001B4 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000001B5 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000001B6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000001B7 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000001B8 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000001B9 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000001BA System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000001BB System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000001BC System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000001BD System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000001BE System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000001BF System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000001C0 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000001C1 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000001C2 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000001C3 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000001C4 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000001C5 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000001C6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168 (void);
// 0x000001C7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC (void);
// 0x000001C8 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57 (void);
// 0x000001C9 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF (void);
// 0x000001CA System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73 (void);
// 0x000001CB System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90 (void);
// 0x000001CC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2 (void);
// 0x000001CD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18 (void);
// 0x000001CE System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815 (void);
// 0x000001CF System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2 (void);
// 0x000001D0 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2 (void);
// 0x000001D1 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1 (void);
// 0x000001D2 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000001D3 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000001D4 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000001D5 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000001D6 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000001D7 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000001D8 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000001D9 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000001DA System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000001DB System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000001DC System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000001DD System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000001DE System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000001DF System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000001E0 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000001E1 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000001E2 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000001E3 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000001E4 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000001E5 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000001E6 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000001E7 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000001E8 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000001E9 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000001EA System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000001EB System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000001EC System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000001ED System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000001EE System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x000001EF System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x000001F0 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x000001F1 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x000001F2 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x000001F3 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x000001F4 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x000001F5 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x000001F6 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000001F7 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000001F8 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000001F9 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x000001FA System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x000001FB System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x000001FC System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x000001FD System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x000001FE System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x000001FF System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000200 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000201 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000202 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000203 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000204 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000205 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000206 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x00000207 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x00000208 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x00000209 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000020A System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000020B System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x0000020C System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x0000020D System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x0000020E System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x0000020F System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000210 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000211 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000212 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000213 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000214 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000215 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000216 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x00000217 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x00000218 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x00000219 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000021A System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000021B System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x0000021C System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x0000021D System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x0000021E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x0000021F System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x00000220 System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern void ConsoleBase_get_ButtonHeight_mCA27289A3F104543F20DC06B91A31AB5B6086CBF (void);
// 0x00000221 System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern void ConsoleBase_get_MainWindowWidth_mD5DF934DE521C4BAC2D8E5D192C99F671CE9A932 (void);
// 0x00000222 System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern void ConsoleBase_get_MainWindowFullWidth_m34B6E4A7E5F46D4BA5C4F6776BE41F81A6EC0708 (void);
// 0x00000223 System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern void ConsoleBase_get_MarginFix_m62D65A386A3E082B094188A8085C0E70EB1C7E30 (void);
// 0x00000224 System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern void ConsoleBase_get_MenuStack_mDC8955F1DC5D449DEEC9E2B9BCC9EDC1ADACB8EC (void);
// 0x00000225 System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern void ConsoleBase_set_MenuStack_m51033E1F07C530521C27F170233FD177D22D45C0 (void);
// 0x00000226 System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern void ConsoleBase_get_Status_m63CCCED1FBDF986534230A863D3CB49D3D19B644 (void);
// 0x00000227 System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern void ConsoleBase_set_Status_m395D6D86AE7C62F043B9B4AB438CF39F7216CFC3 (void);
// 0x00000228 UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern void ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD (void);
// 0x00000229 System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern void ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9 (void);
// 0x0000022A System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern void ConsoleBase_get_LastResponse_m681ADA0CE897B024487F8F4C50260F71772D9386 (void);
// 0x0000022B System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern void ConsoleBase_set_LastResponse_mA472606A78F4CA3BA65B94644F4B5DD7F70C532A (void);
// 0x0000022C UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern void ConsoleBase_get_ScrollPosition_mF3756B3BDEB874C262A7EB7178080C045F4CF084 (void);
// 0x0000022D System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern void ConsoleBase_set_ScrollPosition_m896C935B5961450A7221C74985844A8A685D716C (void);
// 0x0000022E System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern void ConsoleBase_get_ScaleFactor_mBD5B4AFF1CB11190D92499E2E4FC849F30A36CA5 (void);
// 0x0000022F System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern void ConsoleBase_get_FontSize_m5324E58BF0BA3C20E0F0207DF52ABDAC5836DECB (void);
// 0x00000230 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern void ConsoleBase_get_TextStyle_m42A89F1195B33C7B6EB0AC5B21CF8DCE0B8F24E2 (void);
// 0x00000231 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern void ConsoleBase_get_ButtonStyle_m9FE82D58E7A4DA3A588A0285A160C95CDB18BB1B (void);
// 0x00000232 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern void ConsoleBase_get_TextInputStyle_m4EF1BA3E08B87166B3719AA787380B60D61D185F (void);
// 0x00000233 UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern void ConsoleBase_get_LabelStyle_mCC3768907195DE623404701909A36C7A81D5F070 (void);
// 0x00000234 System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern void ConsoleBase_Awake_mF527AF51A14B16534EE6F207FE429271F30DFCE6 (void);
// 0x00000235 System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern void ConsoleBase_Button_m227EA5755011461613164FC2E652AF490057B2C5 (void);
// 0x00000236 System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern void ConsoleBase_LabelAndTextField_m51ED01D9C65067D9BAD30A37EB346E45E9C71151 (void);
// 0x00000237 System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern void ConsoleBase_IsHorizontalLayout_m7AF2417167256558F464D95D8E37519E2C9ED9C1 (void);
// 0x00000238 System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern void ConsoleBase_SwitchMenu_m722B511130E38BEB0B82ABEA06EFEF7A8FCA5AAD (void);
// 0x00000239 System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern void ConsoleBase_GoBack_m2F521BD6668698924A6ADE7B8A33638C79FA0685 (void);
// 0x0000023A System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern void ConsoleBase__ctor_m6A9701FD37DF150A0944E6D6011C8AF2F1D01224 (void);
// 0x0000023B System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern void ConsoleBase__cctor_m638237AA15700C2E8B101F9D9F60E12C0A4E0734 (void);
// 0x0000023C System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern void LogView_AddLog_mD1DEEAD86B0AAF388524DD3C301212D187094FEE (void);
// 0x0000023D System.Void Facebook.Unity.Example.LogView::OnGUI()
extern void LogView_OnGUI_m09AF694B32089C3DE63F8AF0579666B99080CD87 (void);
// 0x0000023E System.Void Facebook.Unity.Example.LogView::.ctor()
extern void LogView__ctor_m3D72776C6D2FF8290D85E3DC714F7F63120A1FB4 (void);
// 0x0000023F System.Void Facebook.Unity.Example.LogView::.cctor()
extern void LogView__cctor_m4F29B617139DDAA7AA6B4E0E65200A239709F313 (void);
// 0x00000240 System.Void Facebook.Unity.Example.MenuBase::GetGui()
// 0x00000241 System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern void MenuBase_ShowDialogModeSelector_m894AD0F5A55FD7C40E8EB1D5C119133B94B7DAD6 (void);
// 0x00000242 System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern void MenuBase_ShowBackButton_m8DD5929D9B8E6A122D061AD13032F284619890FB (void);
// 0x00000243 System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern void MenuBase_HandleResult_m5B0AD1ECB3F5776D34528CB77C81FFDDA7E5AF3C (void);
// 0x00000244 System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern void MenuBase_OnGUI_mE9F148A8B87F4A735309EECFC1EF7BFD30966C00 (void);
// 0x00000245 System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern void MenuBase_AddStatus_m3877A7CBA333AB0EFA6FFE5B92820AC6013167D8 (void);
// 0x00000246 System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern void MenuBase_AddBackButton_m4DB356E0DC7A6DB5A383D1A2F12AE90E2F2B875A (void);
// 0x00000247 System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern void MenuBase_AddLogButton_mAE4A3F4F817922DEBACB7CD1CEC37EB60736EB5F (void);
// 0x00000248 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern void MenuBase_AddDialogModeButtons_mA91FE27EE4BB71509169F09212E6C11A4CFB390C (void);
// 0x00000249 System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern void MenuBase_AddDialogModeButton_mFAAB203118721B880409F7BD23F7CD59FC64BB04 (void);
// 0x0000024A System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern void MenuBase__ctor_mF263A4EB96A4DDE5C9B1EC63E949FC693D41DCBF (void);
// 0x0000024B System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern void AccessTokenMenu_GetGui_mE1DC403055C5043F64F350888C92C0178311F26E (void);
// 0x0000024C System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern void AccessTokenMenu__ctor_m6B70B5C4239CF6ECBADA3DF33BA8AB560E4A21FB (void);
// 0x0000024D System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern void AppEvents_GetGui_m3FE779C3A3BD7049EF051CF07CEEA3C065FE83B8 (void);
// 0x0000024E System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern void AppEvents__ctor_mAC847A17BF516137EA59B65BE56FBB8A7B2B9C7A (void);
// 0x0000024F System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern void AppLinks_GetGui_m95A9E96EBB6D94C066E4962BAB3354C897750129 (void);
// 0x00000250 System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern void AppLinks__ctor_m53275ACDB57B9C32663DB8213897900E3B6321EA (void);
// 0x00000251 System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern void AppRequests_GetGui_m64FAC11162FA76E52EAFF4FA458F391369B31BE4 (void);
// 0x00000252 System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern void AppRequests_GetSelectedOGActionType_m1197DC4A0249B09AEAFF25BFFF8EA2FD9C3BB813 (void);
// 0x00000253 System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern void AppRequests__ctor_m66CE340D69DB0439EDF947359A0E8ED191D90818 (void);
// 0x00000254 System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern void DialogShare_ShowDialogModeSelector_m4A94F284A7AA03D20D7EF06817ADDA4F4D22BC7A (void);
// 0x00000255 System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern void DialogShare_GetGui_m86B39867A307890A53383C8B1BF7B9724A81384A (void);
// 0x00000256 System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern void DialogShare__ctor_mF2E81A32E740BE382EA48FD100DBFB068B400142 (void);
// 0x00000257 System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern void GraphRequest_GetGui_mC5B2D657033092B6A9AA2B7F8A3BFDD433397E2E (void);
// 0x00000258 System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern void GraphRequest_ProfilePhotoCallback_mC9E8B21A2A5EDDDCB2BBC83851077A147D889409 (void);
// 0x00000259 System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern void GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904 (void);
// 0x0000025A System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern void GraphRequest__ctor_m2E200B7AF18E01CA23EA4442A4533773D9C8AA08 (void);
// 0x0000025B System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::.ctor(System.Int32)
extern void U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A (void);
// 0x0000025C System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.IDisposable.Dispose()
extern void U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B (void);
// 0x0000025D System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::MoveNext()
extern void U3CTakeScreenshotU3Ed__4_MoveNext_m5CC1B979FCBFA45CC4DE44556BFA5DE5FF8E5C47 (void);
// 0x0000025E System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8 (void);
// 0x0000025F System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.Reset()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674 (void);
// 0x00000260 System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B (void);
// 0x00000261 System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern void MainMenu_ShowBackButton_m255ED4296523851CCDD84E6D431C2F4FB95298D8 (void);
// 0x00000262 System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern void MainMenu_GetGui_m10B78924EFA55AAB5117C14F7D3BD9E29AF3C68C (void);
// 0x00000263 System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern void MainMenu_CallFBLogin_m1B0369D4D6E9E584DD0757FE1EE3614F782138EC (void);
// 0x00000264 System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern void MainMenu_CallFBLoginForPublish_m3CC64106CA0E41CBF6955979D91795068EE5050A (void);
// 0x00000265 System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern void MainMenu_CallFBLogout_m3B980F09EC74BF2E909272DBD335DF47672F1596 (void);
// 0x00000266 System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern void MainMenu_OnInitComplete_m3E577DB4E98D84D3102B6C4ABECB5EF469191359 (void);
// 0x00000267 System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern void MainMenu_OnHideUnity_m3977B8A15B9DD23E5E143E0D499FB66506A77606 (void);
// 0x00000268 System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern void MainMenu__ctor_m395A9D3C110C6F3E6A0671389C35A6B8B79F3F32 (void);
// 0x00000269 System.Void Facebook.Unity.Example.Pay::GetGui()
extern void Pay_GetGui_mCB2EB9638D21C60BEC97A2BE50BB8C652170BC70 (void);
// 0x0000026A System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern void Pay_CallFBPay_m7C6E3F32EA8A70293A14F8F4F8E8D51777F31F5C (void);
// 0x0000026B System.Void Facebook.Unity.Example.Pay::.ctor()
extern void Pay__ctor_m022913AB693A521C412E05FE2782B5D07A6FD689 (void);
static Il2CppMethodPointer s_methodPointers[619] = 
{
	GUI_Demo_Start_m6753CC1A6D2044AF9C77B4C7273F94304B68E462,
	GUI_Demo_OnGUI_mEB66A73B367C1BCF6D9A1BAEC0561603C34141B7,
	GUI_Demo_DoMyWindow_m45D8A48E8733E60FB231419B8874EC28E0F3A1E7,
	GUI_Demo__ctor_m19ED6E96FF715751F68BEE7444656308611A14A3,
	switch_scenes_Start_mA8D9A4D44D99D2B58CBBC518A780740D58721009,
	switch_scenes_Update_m875654AADB9D17BE5547EFD83A0F955058BF56AC,
	switch_scenes__ctor_m989FDA83C25E947189EF9847328E722852C4CECD,
	switch_scenes_U3CStartU3Eb__1_0_mAFDD8E35E748180891E696AD071066A70FA41AFF,
	MSJoystick_get_rectTransform_m86059C73213015238D5DE0EB82CAE49E86341C27,
	MSJoystick_OnBeginDrag_m8DC24E6BF83E8118D672B7942A14AD5402F68617,
	MSJoystick_OnEndDrag_m8CFB0EEF70FE2F0135125C7B4FCBB980A8B110F1,
	MSJoystick_OnDrag_mBB1DF2AE1C7D5446B6AC2913AA70B888DB534240,
	MSJoystick_OnDeselect_m7C6D1251E80BE6A633A999FEC54400D605CDB18E,
	MSJoystick_LateUpdate_m2A1D2EC9EA4B3E1AD5D9B3B0132B93ABE211EBE9,
	MSJoystick_SetAxisMS_m96E70CC7737FD0A992DA8915F6EA8C92970D0603,
	MSJoystick_UpdateJoystickGraphicMS_mC59AF89D40E7A8BF9D416E51CD84C9A5DF38FC0B,
	MSJoystick__ctor_mBEC734443D847C8E216BA5D408ABE57BFD36EED2,
	MSJoystickController_Update_mDDEBB4361EF8411673E7991FC64671E56ED672C4,
	MSJoystickController__ctor_m373271A7733AB522E66BD52E838B77462B16C4E4,
	AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01,
	AudioManager_PlayMusic_mB50D212EBE632BF1E95380A7F222B043A0FBFFF1,
	AudioManager_PlaySFX_m13FCA19E8F5B2A013718669280D1AABFB4767E03,
	AudioManager_PlayAudioFromTheRocks_m6689B17CB6ADAB172E096EED74865A5EC27EDFCD,
	AudioManager__EFX1Ajust_m3C460A48D6AD2DEC1BEB668AF6216BE8F9B40D64,
	AudioManager__EFX2Ajust_mB71A9EE34551F678543829A5E17F04DC611BA1FA,
	AudioManager__MUSICAjust_m030B0B836806AD125659B4F3AC21995DF6480140,
	AudioManager_SaveAudioSetup_m26BCA901ACDA60922DFB71CD7972C196673358C2,
	AudioManager_LoadAudioSetup_m0C530D84037829EC041ECA7EC1DCC0F5BDF9F60C,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	AumentaVelocidade_Awake_m40F7D7B352D7A020A8A8B82EADF9AC76913A2747,
	AumentaVelocidade_Start_m6722AC7BA8ECC2700A17C350FD687BAEB5FB118F,
	AumentaVelocidade_Update_mF42566D568DEEB98FC4ACD71F81592DE93D33525,
	AumentaVelocidade_recebeVelocidade_m07F2F6EDF044AFBC7CC166574078A3D0A8FD1B41,
	AumentaVelocidade__ctor_m615D73D75460DE5722CCAE05FFF8C18BE04417A2,
	Calculadora_Awake_m23D5ADD5972AC2B1E0C90A497BDE1D4445ECB76B,
	Calculadora_operacao_mBACB4F365965A6619DCFEB7D774375F68B151703,
	Calculadora_sortearPorta_m436BCE65D81671B44F202B4726626FEE63CF6086,
	Calculadora_executaOperacao_mC0CB12F0B3DF3F729BF9083DDD6D37BF91A0E802,
	Calculadora_respawPortas_m896CA76CB113C76DB7F42A50971F174B69753BAA,
	Calculadora__ctor_mAEC7C0155367249D2C85E34479B73BA898F8263B,
	Config_Start_m4C931C9FC5362D375490C765CE1096A19CBD6E61,
	Config_Update_m6A1B69535117A9999608EAC7B7A567448561EDBB,
	Config_soundOnOff_mF16B48B5C86113E4149ECDF10FF2F828FB827DD8,
	Config_musicOnOff_mC5E5930726300BE1F3FFCAA24DAB06C3C4E8E0F4,
	Config_desligaSom_m4A7739BD5EE36C48B247820A9A2EB456DAF42715,
	Config_iniciaAudioMaster_mAA759E925018D03DFDCCC073A181471BBDF989E4,
	Config_VolumeMaster_m88A7B905D815FD3E07F69B36FDB0E52DC05F078A,
	Config__ctor_m74DB672E7C9821A5A767CDBA9BFA3060710B4712,
	Configuration_Start_m8FE82130A1A210D422327BC611C53585B8C8D3F8,
	Configuration__ctor_mB313DBB5812D634A19CC8B53D9A873A709B64C96,
	MenuPrincipal_Start_mE4E6BC42308F78D60A85F4364869150336D4ADC1,
	MenuPrincipal_start_mA2F96F922A5ADC5B3CB5B372A96DBEFAEFD3869B,
	MenuPrincipal_aprender_mAE6BE86BA7CBB863388C5DE4EF3551525F42E381,
	MenuPrincipal_ranking_m1839D883FC13CE59541D3C9E1F1EB29D13CF51BB,
	MenuPrincipal_ajuda_mED87B60E79FB17401F323D8492DA3437286C81C0,
	MenuPrincipal_sobre_m5E81D753929703812661C962C0870AA410FE89F2,
	MenuPrincipal_config_mB2936436B1A8A4E5A26390A1D3BF7600E65D4648,
	MenuPrincipal_returnMenu_m9342D2C134C48F015E946656161AA49E17DED1FB,
	MenuPrincipal_adicao_mBD7671A5A8873FA1521890E76AB1FA38847F88DB,
	MenuPrincipal_subtracao_m8B1890B99B664E1DA3ED6722831281282FB5D4A3,
	MenuPrincipal_multiplicacao_mB58F8CD8E1F4103B12499634DED9701CF9ECB6E5,
	MenuPrincipal_divisao_m9F700F5DA18A661CBD2F6DE181A03B9BD428D9A5,
	MenuPrincipal_tabelaErros_m5CF7FA7A55BA531AA3CCC9756BCBC8FDE46C1872,
	MenuPrincipal_iniciaAudioMaster_mFBCE540A1C053BA95407E042417E3AD3B9811701,
	MenuPrincipal__ctor_mB9DE46999E38C9C587A287D85E6DD1977760C1C4,
	MenuSceneManager_Awake_m9F9DBA447541637778C280B595F2E5E672C5AD86,
	MenuSceneManager_Start_m501507B088BBF8240CFD9F13A8D15E984E5AE275,
	MenuSceneManager__Pause_m872B5CEBB89106EF78210C9ABB3739F7CA99B360,
	MenuSceneManager_cover_mDE1D247160985D39B9137BAAAC035C5D3648B634,
	MenuSceneManager_abrirButtons_m0E230A9CB0D6E01ED2FE6BA8BEF04DDD8004D6FB,
	MenuSceneManager__adicao_mDD21C1D8CC25B320D1341175D73F063DE48DDAC7,
	MenuSceneManager__divisao_mDD6441FB175EEC7195D96FBBDB2362570F0CD7ED,
	MenuSceneManager__multiplicacao_mA08DB4A718A1EF64EE4913B0E5C74228594275DE,
	MenuSceneManager__subtracao_m3C011C9E9ED9835B9EC950DC2AF7A1600A678BBD,
	MenuSceneManager__botaoIniciar_m557D4552F98EF7C0AFCC599A5569860B42A7295C,
	MenuSceneManager__retornarMenuPrincipal_m85EC7C551CF14D18A2C9039C31DB36C3F2334BE8,
	MenuSceneManager_desativaButtons_mCD3A70567B4CFFA7A73931EA11F5B72260DB172C,
	MenuSceneManager_ButtonsAfterGameOver_m8393D2A7F5F4741F7FE6FD5438EB5A5C7DF83DD2,
	MenuSceneManager__ctor_m57DDDDBCD7568C95037EC0400634AFE698AB6682,
	U3CdesativaButtonsU3Ed__26__ctor_m6D0845F09C3DD6F2382E3E135F8BBFC0EEB214C0,
	U3CdesativaButtonsU3Ed__26_System_IDisposable_Dispose_mBB55A8D1B44EDF499076B78858309F23D61A9525,
	U3CdesativaButtonsU3Ed__26_MoveNext_m6DBEB1AF65FD77E5D639AD985060815C48E0D948,
	U3CdesativaButtonsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B45F07001BEE68A5A2D126B2608E5796864B35F,
	U3CdesativaButtonsU3Ed__26_System_Collections_IEnumerator_Reset_m77A1EEA00659124B2624E69ACA62E31727B9B047,
	U3CdesativaButtonsU3Ed__26_System_Collections_IEnumerator_get_Current_m75FEC85CC6325A6F33756AF8C817732265CB7681,
	MovementButton_OnEnable_m1D8C0C1BC9B05093E70202B50D6E9F1E51D078E4,
	MovementButton_botaoEsquerdoClicado_m64BE766EE61A55BB1A33EEC57B24721C3CBC8F63,
	MovementButton_botaoDireitoClicado_mB1B1CF9646789CBA3682F2B25426E52DE0114F7C,
	MovementButton__ctor_m27C3B4408F63FBAD552853EFF68F4138B4AA5B1D,
	PanelManager_OnEnable_m09547ED27D87F696B41F1FAF5EFC3826F3204E2D,
	PanelManager_OpenPanel_m55C75D9E37F2CDE8B1670FBF979329E5D6D13E0F,
	PanelManager_FindFirstEnabledSelectable_m59C12281C9EAED20F4FBB9C820D4EAD2C2DA117B,
	PanelManager_CloseCurrent_m20DE00E5BAE15FA1D4A052E555A3CC5C58A6A695,
	PanelManager_DisablePanelDeleyed_mB120A21FD25F2168724F35B9FAD134487EF0507A,
	PanelManager_SetSelected_m33A82F0C75C5439BE6C1FEE4B93BF4E3949FE882,
	PanelManager__ctor_m945C0470B90313D67EDCED0D97AFADB78A190AFD,
	U3CDisablePanelDeleyedU3Ed__10__ctor_m3595FCFEB5B1694DEAE36303E82C43199E309861,
	U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_m37D3EA493F4986A2E0A6263B72E96EF00126946B,
	U3CDisablePanelDeleyedU3Ed__10_MoveNext_m7BFBF535CE4EBEABFFB4B592D5BA81B83152B2AD,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82D3317E9507246F91AABBA087291CE3DFAD9B4C,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_mCCE2A629729D15A72499C8B3EBE2380829AA7291,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m271F5CC65D4EAF362EEEB37323D10ABBE73D0554,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_movimento_mBE5B3B5A466F35ABF7FA41DBA8A0CA8D5D9ECEDC,
	Player_OnTriggerEnter_mE273002981E5225ABE0C891B825A78BB9FE905E6,
	Player_OnTriggerExit_mF514DAD7438D7BC1290C7340EAF245B55BFC33FD,
	Player_playerDown_mF0D2067EF7836C4E39914F27DAD0B0EF421D72FE,
	Player_AumentaVelocidadePorta_mCCED915CE6BF6FB6B32A91FCAE101A499E69B86C,
	Player_Idle_m10EF471CCAE716DABE927CE3BE7F0FF0B596F01C,
	Player_left_m2911D1AEE81C4AE5E28FC0C7FFB1710E8018CE28,
	Player_right_m10034C7720491D3C099DBC65DC02C1D0B712C722,
	Player_Celebrate_m45701906C9D71AE7B9A5F336EDD18BFBDF5D628B,
	Player_stopCelebrating_m957315807065DAA64622C1F8975C7077D1C065BF,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	U3CstopCelebratingU3Ed__18__ctor_m2B1F184CFA5F940DB0531F9D09E81BB158921BC2,
	U3CstopCelebratingU3Ed__18_System_IDisposable_Dispose_m1D2A0522885D354FCBD633ABFB42E773D9FD3AFD,
	U3CstopCelebratingU3Ed__18_MoveNext_m83B9F9EAD944D67A29F1621B8125407310159820,
	U3CstopCelebratingU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E3684B231F429F7834F1A37BD9F129BC2700322,
	U3CstopCelebratingU3Ed__18_System_Collections_IEnumerator_Reset_m5B109D0CB07C5A23D4DBBADFEE703C11C9F34CF7,
	U3CstopCelebratingU3Ed__18_System_Collections_IEnumerator_get_Current_mA5E1A11D26CCF8E761C9FED52BD98BA4417A9E5E,
	PortaSingle_Start_mBA8092B4EB8A11E845A6B147B87E0950AEEA217D,
	PortaSingle_Update_mAC59D0AB0FB1A54E122E8B62906E13AB6194B335,
	PortaSingle_limite_mB09FE3AD863C042CC90A80BEE18BAEE1F3852FEA,
	PortaSingle_gameOver_m9F6E2FEBC61577868C23FF7FF314A22C4B219B22,
	PortaSingle_playAudio_mDBAE5BE029D89823903C7F7E310511A969462B64,
	PortaSingle_mover_mD0CDB1A60DC88FA831D7819F3059FF09269F250A,
	PortaSingle__ctor_mC83950417ED214EFD124C8F04F15E3EA8E7DDFAA,
	Record_Awake_m62E5E5F979F3177ED0BA78440FC11D9DB80F4228,
	Record_Start_mDB280E1F77A30CEF4573EFFC04837C4839F3732C,
	Record_atualizaRecords_m65BAE938E2C8896371730443E4AB21373A654436,
	Record_receberRecordsGravados_m6A28CC19F5E2F17C5979500498165016DB2A30E1,
	Record__ctor_mA1FAB02A3F3CE8F3B3948E834F06AC4DC86F90B4,
	RespawManager_Awake_mB0515F0438AE05921BA89C08877CE8D0638F7D4A,
	RespawManager_Update_m23FC8F81B42DFF8E52FC830E0ADF306FD3A4045A,
	RespawManager_InstaciarPortas_mC5101431BF494C97439262072234B28DF6E80E82,
	RespawManager_inicio_m86AB1BCBED76A5594C4F0E01B63669994708CDAF,
	RespawManager_colocaOperacao_mCAB436219BC9022C5267F5D35768F130A4D510B2,
	RespawManager_SetFireworks_mA481107AD6E9ED70C1AD5B130C0CBA859931F043,
	RespawManager_DestroyFireworksClone_m4F661F09D8C0CD07200917DD84BE408F5E802A45,
	RespawManager__ctor_m7EBB97E5DD07F7D003DA64C2865249A82DCD42D5,
	U3CDestroyFireworksCloneU3Ed__13__ctor_m6EA62449DB1045BD90493D1343B61DB9E1491771,
	U3CDestroyFireworksCloneU3Ed__13_System_IDisposable_Dispose_m76B65423FDDD8F437E158D099BD31C4FB3287153,
	U3CDestroyFireworksCloneU3Ed__13_MoveNext_m05339AB817598C6E3DF82C01C70117AA257982AE,
	U3CDestroyFireworksCloneU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6204070EE7BDBBA40C06BB1E8CB25E58F0862EC,
	U3CDestroyFireworksCloneU3Ed__13_System_Collections_IEnumerator_Reset_m1AFCD57B6FF2F507AD153F5889D9F145BDCF0C48,
	U3CDestroyFireworksCloneU3Ed__13_System_Collections_IEnumerator_get_Current_m8F01E4889F4FCA7499F0D71B77EAF5C9A5805EFC,
	SliderBarsConfiguration_Start_mDACC334594F936D4C8C4B5470EE3714E5CA03968,
	SliderBarsConfiguration__EFX1Ajust_m02A8C6BCC50506E8576A169A8E16FE34CE243DAF,
	SliderBarsConfiguration__EFX2Ajust_m5C9AE932486F80205DB60BEEE5F887C25A3F5A70,
	SliderBarsConfiguration__MUSICAjust_m5D8979F66BFC484A8C9DA6DE279DD1815C468DFC,
	SliderBarsConfiguration__ctor_m87E00FD51DA636E7FE0166756FD79C37DF085EE2,
	TabeladeErros_Awake_m8E9DB5DF4E7AB2A74FA6E86C66F781318236628E,
	TabeladeErros_Start_m9D2CE08099407C5F65A682647EED8DA68B47D7A6,
	TabeladeErros_gravaErro_mC2641669F81EC22C2E30BE1C6A6A36FA8F28B440,
	TabeladeErros_atualizaTabela_mA600F3CA7EEC17A5CE082A8C1D2BC56B7441ADCF,
	TabeladeErros_qntMulti_m2FF7DC65727C37FCB43D30BEE91FF925365E4222,
	TabeladeErros_atualizaNumeroVezes_m5214D1A50122E6113E91A9CF8893F5156BFA53B5,
	TabeladeErros__ctor_m94E0CDE2832716CE7C5739E1B8F7BF7F81695D11,
	TransitionSceneManager_OnEnable_mB8383EDD9E2CA55B8132288ED9446BC5C0190CEB,
	TransitionSceneManager_Start_m7F56CD1005EBA5D601166C0940175F9E83A34209,
	TransitionSceneManager_ChangeScene_mB4CE23BA6C984733AF0938F5350EC68CDA7FD092,
	TransitionSceneManager_Countdown_m8CB57B62D076BACDF24FCF1C0AB5AC192009C878,
	TransitionSceneManager_GoMainMenu_mBBDC80B928BD3B5ECF93897045F2F64303D92654,
	TransitionSceneManager__ctor_m119EB7FAE132C33115802D130B4A261B4F778FB4,
	U3CCountdownU3Ed__7__ctor_mEF6C97BDAA7E1AE8D8C4890CB75A3C4BBF147EE2,
	U3CCountdownU3Ed__7_System_IDisposable_Dispose_m5A3E5BD204C52BEF7DEBD92BA3C41FF148B6F732,
	U3CCountdownU3Ed__7_MoveNext_mEF4C27693E5B14C6B2EE2B43E6CDD014001C2E99,
	U3CCountdownU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m665A3EC933D039B9EA00E33895894CE8FBEE3E48,
	U3CCountdownU3Ed__7_System_Collections_IEnumerator_Reset_m68C5B82123EEA040131BE0F665096885E9B05E15,
	U3CCountdownU3Ed__7_System_Collections_IEnumerator_get_Current_m171E67D50838CCF5EEC358E14BAB0C97BE86C679,
	U3CGoMainMenuU3Ed__8__ctor_m2DA2014F66926CB509AA23D9E929177BA14F0DFF,
	U3CGoMainMenuU3Ed__8_System_IDisposable_Dispose_mB8E4FDF2F51C30F221B1E9956C47E449599321DA,
	U3CGoMainMenuU3Ed__8_MoveNext_m022F3B59B165FB7F417FC0ECB5EE439BA36ABC06,
	U3CGoMainMenuU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FCA6EC72AD4115E4B1DBC25A2E3AB1110319C8,
	U3CGoMainMenuU3Ed__8_System_Collections_IEnumerator_Reset_m06E399196E0E93903DF007223CAE53153045715B,
	U3CGoMainMenuU3Ed__8_System_Collections_IEnumerator_get_Current_m2F461B1994A9B36D30218553B2F1BC370A5E931D,
	UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30,
	UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E,
	UIManager_mudaCor_m392E5B0AB40F79F245487AABEECF59F0FD2B0336,
	UIManager_limpaPainel_mD45892622E19C92309518A11EE110BE5B6E98EB5,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	BtnAdicao_Start_m1F800BA9C2CAB4A5C859399A8976FD477817A2EA,
	BtnAdicao_Update_m04096539BF51ED67D00A4E623A09B01CD39CFC84,
	BtnAdicao_adicao_m28370DAD697E0B171111870714E30A02ABAFEB5D,
	BtnAdicao__ctor_mEB2F14C5CF5EC08515CBB149FBD3A478434AF70B,
	BtnDivisao_Start_m2A9AA939817E9974D54AEFD7519E732D88D7BE4E,
	BtnDivisao_Update_m6C45D7BB89E3E3C17A264A386EAAB1B4E0CED7C0,
	BtnDivisao_divisao_m20B1663F66531CBAA47199D6613C12BC5825E88D,
	BtnDivisao__ctor_m0C12493AC464B86BACBE0CC3E87654058C553981,
	BtnIniciar_Start_m2864945962A5A944AA96177EF05FAAE9E5063D32,
	BtnIniciar_Update_m92846AB4358845B9A3FC83D0E566C2C5B6E319D1,
	BtnIniciar_botaoIniciar_m907950A94978A88005723623C5AC47F78BFD6DB1,
	BtnIniciar__ctor_mCCA75A8220C237DC37B161BB23E5171198DF2A54,
	BtnMultiplicacao_Start_mC3FC3B13AE4D50ABB7C43C3B8B2F708139372A42,
	BtnMultiplicacao_Update_m68FE120EA46DB373448550F8BA533FDC479F6637,
	BtnMultiplicacao_multiplicacao_mA9A2003AD0EDE113145476CCDF8F5A0F81FC1C5A,
	BtnMultiplicacao__ctor_mEE09D46762BD61B11673EF6722B6B05F93937003,
	BtnSubtracao_Start_m224B196706CEA5B0EB3D99875097D094FE20632E,
	BtnSubtracao_Update_m267F104A1A04BBD3E2E65847EA5B5F40354A4683,
	BtnSubtracao_subtracao_m8909D43314C8559E2911490196D631A54F1F8CCA,
	BtnSubtracao__ctor_m608907D9891883BE42E4FC0FDA94572B6A6C031A,
	Grito_Start_m9E58F564659F310C38F0889A1E80BBB767F3D035,
	Grito_Update_mBAAC1010A9069DDA82063A1829DAE00E99A5E117,
	Grito_OnTriggerEnter_m67449F0CE21025681D703E7722ADCD4F4E1BACE3,
	Grito__ctor_mB8BCD4716FF58CF0C74291C0A5982B6685E32019,
	MovementManager_OnEnable_m560AF40B2966E61EF969CB9AF6136D960F37C856,
	MovementManager_Start_m5E101C7CB4F977AE843D5B15FAFA810A0F54EAF1,
	MovementManager_Update_mC3987BFB04EFAB5132C15588F96118333F36E2B5,
	MovementManager_receiveReferrals_mCD5C7593D184AE70CCA9BDB177D6B4873F34355A,
	MovementManager__MovLeft_m6EB43AAEFDFD99B462AA0BB598CE3FB78295330D,
	MovementManager__MovRight_m68040F21387EEC52B630A2A7E88C99665E0980F0,
	MovementManager_Idle_mA77605339804F73BE47A82769DD83F6F1C4F2174,
	MovementManager_left_m3840AE96E57E7FFE6E88983143A13632C7F1231C,
	MovementManager_right_m3738BD4B9671ED45656FE50BEF8B7A31DB16F481,
	MovementManager__ctor_mC9B41C1DB50F23ED5238525C50B434B735E442CF,
	PlayerLerper_Start_mE9FC9BB378D367D0B28992858B0B1A0C9CEF4AD9,
	PlayerLerper_Update_m960EAEC5D41A7D386A62FA732EBF3F43C1C1CB9F,
	PlayerLerper_RepeatLerp_mAF6F7C368D07CC5BFF03409BFD042E1F1E5BC18E,
	PlayerLerper__ctor_m4643F8FAB2DE08280350185D49ADEAB265F076C8,
	U3CStartU3Ed__6__ctor_mC70C71B465A4ADB0257AC9D2EEDE9AE9B3746ACA,
	U3CStartU3Ed__6_System_IDisposable_Dispose_m4C41C16D10F9FA73F4FD339B55A51FC5ABABBDDE,
	U3CStartU3Ed__6_MoveNext_m16442D90D77CA0218E9B2E4013D022231C8854E3,
	U3CStartU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD6240CA3CE7684B9EA3751110F8F78DEE445EBC1,
	U3CStartU3Ed__6_System_Collections_IEnumerator_Reset_m3E0672E0EA91A827DC3278894F085570A776AECA,
	U3CStartU3Ed__6_System_Collections_IEnumerator_get_Current_m04CC86276A34D99E92731B5927E09A25F40464E7,
	U3CRepeatLerpU3Ed__8__ctor_m3C1CD7B493CC82965051D16CB07D77FB35BBC52A,
	U3CRepeatLerpU3Ed__8_System_IDisposable_Dispose_m87FD269733B3F423CB4EDBE4C55DB51FDBABA80B,
	U3CRepeatLerpU3Ed__8_MoveNext_mA716C3E872A1773F5485193FD771D31F8232D600,
	U3CRepeatLerpU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC16C1751F17D4FCB8E57A54DA4A08AB2C73BA011,
	U3CRepeatLerpU3Ed__8_System_Collections_IEnumerator_Reset_mBEF8E1F84572D51241D6A0145F5416CCC8411DFC,
	U3CRepeatLerpU3Ed__8_System_Collections_IEnumerator_get_Current_mC0544794445FD91614B7905B78931CF027A0A466,
	ReturnMenu_Start_mF0D308EE802E0A442688096F2B76DD03DE69B267,
	ReturnMenu_Update_mB02E84594338DF6558AE453FBDE13606B1BC4642,
	ReturnMenu_retornar_m2C1C3D277930CDF5CF6DF30411E809EE0E26AC07,
	ReturnMenu__ctor_mAB307DE82BD6BB0F85B38C071444A343C1F737D1,
	Roda_Start_mDB4CD9A767A5ED3AFEC68E3760E580183E3CBAEC,
	Roda_Update_m0899ED8351B5513B8B2C4BDDD3640C9EEC71C3AB,
	Roda__ctor_mA06EABC969CDBAC2D00D84CAA384C4F10450A968,
	VirtualJoystick_Start_m54B90FE037FDFD13244077A91EEAFA888ED6BBC0,
	VirtualJoystick_OnDrag_m3DC97C2455C85EA1AAAEE893BF6107590F32223E,
	VirtualJoystick_OnPointerDown_m405614EFCBD672DDE5F07289E5ABDFEEC13B86AB,
	VirtualJoystick_OnPointerUp_mFE9F180134AEC17FFEF8AC503B6880D6B1CCA79E,
	VirtualJoystick_Update_mE5916F6E2B087A0AA5747900E675590F4F9D877F,
	VirtualJoystick__ctor_m13CD0570C7630D04EAB8081592F05590DCF5F2D5,
	CameraLogic_Start_mA116341F9A1BB67B349AC114E3F0D34623E611C9,
	CameraLogic_SwitchTarget_m25361D85881175425D3052C5CF29FEE0E6898BC4,
	CameraLogic_NextTarget_mFA284EE569009D52295112739EA96E85C9D3C0DC,
	CameraLogic_PreviousTarget_m288C0528639524275FE571D019ABE4ED86240C10,
	CameraLogic_Update_mAAD604ABB9F03AEA994EAC4FE606C8CB2A7E40D5,
	CameraLogic_LateUpdate_m17F5CF81B28F1FC71D73BFBE93161BDD37D77C4E,
	CameraLogic__ctor_m8ED9E6EEAEB3ED54C9277289D85DBB4815A34A66,
	Demo_Start_mA96D71BE495F276B026F0223EAAF21A1B4C626E8,
	Demo_Update_mA20CD8F8E6C14018B5051CFC4624F1559E45BA40,
	Demo_OnGUI_m43E8559CBC9366A8702DE5BE2066926C57825717,
	Demo__ctor_mA1BCCF87BB86D1772EABA9E89B452959AA579645,
	SimpleCharacterControl_OnCollisionEnter_mDD3D6DA9717BD197BBB64898AD5FCA816187240A,
	SimpleCharacterControl_OnCollisionStay_m3AEEB2C953BAB6C720598C0868A7976EC92F3355,
	SimpleCharacterControl_OnCollisionExit_m806BAB2C01B7CF0B0581CA58736B1342E4CB6C00,
	SimpleCharacterControl_Update_m5681DBDA0F1B7A9546A62F43F557F93A1E1E48F7,
	SimpleCharacterControl_TankUpdate_m963FB4DCA9FC24A94405C841CD2A2D8D62693DD0,
	SimpleCharacterControl_DirectUpdate_m33DCC602CE9F0EF409D4D2ECB4CBC18B3DFA16AE,
	SimpleCharacterControl_JumpingAndLanding_mE87AB9D0B87B5B13CA45EE8F8C250C8B89E416E8,
	SimpleCharacterControl__ctor_m859C68B295A4AF78ECC97E2AEE6FB88A661F833C,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	playerScript_Start_m861652F32C1A038ECD57E6A298787E67D05979B2,
	playerScript_Update_m8B08A7D12C5CF8DD1219EE6BD4A1323DC9CE0DBE,
	playerScript_movimento_mB3586E846B1D16E993056CFB0D4C525D4ACDE616,
	playerScript_verifica_movimentacao_mA82D4D60C971867B78A7484F366FFB98EFAE68ED,
	playerScript_checkSwipe_mFFA33849CB5E59618B2F9A9BF397F4207F76D5CA,
	playerScript_verticalMove_mC6BF591F9A817E1AE3B086E112840DCC0116BAA4,
	playerScript_horizontalValMove_m805E05D3A65E191125B18E04F5A84D0CCCAA16AC,
	playerScript_resetaDirecao_m05864B552C4CE94CCA042051A14E613B137DE3EE,
	playerScript__ctor_m29F0FA3290E1B828026CA6BABF42D31B8CBBEE83,
	playerpc_Start_mBBABD64DAB510F24E61F30F97FC5617A33247136,
	playerpc_Update_m07A6BB7A5D1E71510CE7B0DDF8094755441C5F0F,
	playerpc_movimentacao_mFDA175C4DC9464C3B72CD12EC24AFB2E4B10DFF0,
	playerpc_verifica_movimentacao_m7078ADA883738F03B97E69C0D744ED8575284BAC,
	playerpc_resetaDirecao_mC1BEB391B038FB4AC396F1FF5D6421BA3BC40C2D,
	playerpc__ctor_m896802B1E7BA521454BD439E86AB564A5B09212B,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_mA27BBC06A409A9D9FB03E0D2C74677486B80D168,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_m510FB73335FCBCBEC6AC61A4F2A0217722BF27FC,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_m8DFB6850F5F9B8DF05173D4CB9C76B997EC87B57,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCD7B5218C81C0872AB501CD54626F1284A4F73BF,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mD2D32B94CA8D5A6D502BA38BDE1969C856368F73,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m59B2488B0E3C72CE97659E8D19BB13C6E0A44E90,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m359EAC129649F98C759B341846A6DC07F95230D2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_m36DE8722B670A7DDD9E70107024590B20A4E0B18,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_mB7D320C272AAA835590B8460DA89DEBC0A243815,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m891591B80D28B1DF2CF9EB78C19DA672A81231A2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m5EF21CA1E0C64E67D18D9355B5E064C93452F5B2,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m4850A72D887C4DA3F53CA1A1A2BEAD5C5C6605A1,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	ConsoleBase_get_ButtonHeight_mCA27289A3F104543F20DC06B91A31AB5B6086CBF,
	ConsoleBase_get_MainWindowWidth_mD5DF934DE521C4BAC2D8E5D192C99F671CE9A932,
	ConsoleBase_get_MainWindowFullWidth_m34B6E4A7E5F46D4BA5C4F6776BE41F81A6EC0708,
	ConsoleBase_get_MarginFix_m62D65A386A3E082B094188A8085C0E70EB1C7E30,
	ConsoleBase_get_MenuStack_mDC8955F1DC5D449DEEC9E2B9BCC9EDC1ADACB8EC,
	ConsoleBase_set_MenuStack_m51033E1F07C530521C27F170233FD177D22D45C0,
	ConsoleBase_get_Status_m63CCCED1FBDF986534230A863D3CB49D3D19B644,
	ConsoleBase_set_Status_m395D6D86AE7C62F043B9B4AB438CF39F7216CFC3,
	ConsoleBase_get_LastResponseTexture_m6D4AB4A8E2B4906C8D20DB62B10DDE0D812630BD,
	ConsoleBase_set_LastResponseTexture_m9495BB5A6A1C0F6C2CA39B3818B06565B1CBC2E9,
	ConsoleBase_get_LastResponse_m681ADA0CE897B024487F8F4C50260F71772D9386,
	ConsoleBase_set_LastResponse_mA472606A78F4CA3BA65B94644F4B5DD7F70C532A,
	ConsoleBase_get_ScrollPosition_mF3756B3BDEB874C262A7EB7178080C045F4CF084,
	ConsoleBase_set_ScrollPosition_m896C935B5961450A7221C74985844A8A685D716C,
	ConsoleBase_get_ScaleFactor_mBD5B4AFF1CB11190D92499E2E4FC849F30A36CA5,
	ConsoleBase_get_FontSize_m5324E58BF0BA3C20E0F0207DF52ABDAC5836DECB,
	ConsoleBase_get_TextStyle_m42A89F1195B33C7B6EB0AC5B21CF8DCE0B8F24E2,
	ConsoleBase_get_ButtonStyle_m9FE82D58E7A4DA3A588A0285A160C95CDB18BB1B,
	ConsoleBase_get_TextInputStyle_m4EF1BA3E08B87166B3719AA787380B60D61D185F,
	ConsoleBase_get_LabelStyle_mCC3768907195DE623404701909A36C7A81D5F070,
	ConsoleBase_Awake_mF527AF51A14B16534EE6F207FE429271F30DFCE6,
	ConsoleBase_Button_m227EA5755011461613164FC2E652AF490057B2C5,
	ConsoleBase_LabelAndTextField_m51ED01D9C65067D9BAD30A37EB346E45E9C71151,
	ConsoleBase_IsHorizontalLayout_m7AF2417167256558F464D95D8E37519E2C9ED9C1,
	ConsoleBase_SwitchMenu_m722B511130E38BEB0B82ABEA06EFEF7A8FCA5AAD,
	ConsoleBase_GoBack_m2F521BD6668698924A6ADE7B8A33638C79FA0685,
	ConsoleBase__ctor_m6A9701FD37DF150A0944E6D6011C8AF2F1D01224,
	ConsoleBase__cctor_m638237AA15700C2E8B101F9D9F60E12C0A4E0734,
	LogView_AddLog_mD1DEEAD86B0AAF388524DD3C301212D187094FEE,
	LogView_OnGUI_m09AF694B32089C3DE63F8AF0579666B99080CD87,
	LogView__ctor_m3D72776C6D2FF8290D85E3DC714F7F63120A1FB4,
	LogView__cctor_m4F29B617139DDAA7AA6B4E0E65200A239709F313,
	NULL,
	MenuBase_ShowDialogModeSelector_m894AD0F5A55FD7C40E8EB1D5C119133B94B7DAD6,
	MenuBase_ShowBackButton_m8DD5929D9B8E6A122D061AD13032F284619890FB,
	MenuBase_HandleResult_m5B0AD1ECB3F5776D34528CB77C81FFDDA7E5AF3C,
	MenuBase_OnGUI_mE9F148A8B87F4A735309EECFC1EF7BFD30966C00,
	MenuBase_AddStatus_m3877A7CBA333AB0EFA6FFE5B92820AC6013167D8,
	MenuBase_AddBackButton_m4DB356E0DC7A6DB5A383D1A2F12AE90E2F2B875A,
	MenuBase_AddLogButton_mAE4A3F4F817922DEBACB7CD1CEC37EB60736EB5F,
	MenuBase_AddDialogModeButtons_mA91FE27EE4BB71509169F09212E6C11A4CFB390C,
	MenuBase_AddDialogModeButton_mFAAB203118721B880409F7BD23F7CD59FC64BB04,
	MenuBase__ctor_mF263A4EB96A4DDE5C9B1EC63E949FC693D41DCBF,
	AccessTokenMenu_GetGui_mE1DC403055C5043F64F350888C92C0178311F26E,
	AccessTokenMenu__ctor_m6B70B5C4239CF6ECBADA3DF33BA8AB560E4A21FB,
	AppEvents_GetGui_m3FE779C3A3BD7049EF051CF07CEEA3C065FE83B8,
	AppEvents__ctor_mAC847A17BF516137EA59B65BE56FBB8A7B2B9C7A,
	AppLinks_GetGui_m95A9E96EBB6D94C066E4962BAB3354C897750129,
	AppLinks__ctor_m53275ACDB57B9C32663DB8213897900E3B6321EA,
	AppRequests_GetGui_m64FAC11162FA76E52EAFF4FA458F391369B31BE4,
	AppRequests_GetSelectedOGActionType_m1197DC4A0249B09AEAFF25BFFF8EA2FD9C3BB813,
	AppRequests__ctor_m66CE340D69DB0439EDF947359A0E8ED191D90818,
	DialogShare_ShowDialogModeSelector_m4A94F284A7AA03D20D7EF06817ADDA4F4D22BC7A,
	DialogShare_GetGui_m86B39867A307890A53383C8B1BF7B9724A81384A,
	DialogShare__ctor_mF2E81A32E740BE382EA48FD100DBFB068B400142,
	GraphRequest_GetGui_mC5B2D657033092B6A9AA2B7F8A3BFDD433397E2E,
	GraphRequest_ProfilePhotoCallback_mC9E8B21A2A5EDDDCB2BBC83851077A147D889409,
	GraphRequest_TakeScreenshot_m6E8E3B5A0BBBBAE2E4124A05607205420B89A904,
	GraphRequest__ctor_m2E200B7AF18E01CA23EA4442A4533773D9C8AA08,
	U3CTakeScreenshotU3Ed__4__ctor_mFDE73BDF39095D5CB04C52943F3F749FC1D96B3A,
	U3CTakeScreenshotU3Ed__4_System_IDisposable_Dispose_mB4A7976DB6C72A46D7BD842E606BA59D639EEA6B,
	U3CTakeScreenshotU3Ed__4_MoveNext_m5CC1B979FCBFA45CC4DE44556BFA5DE5FF8E5C47,
	U3CTakeScreenshotU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4BFEAC3397C268E3CE931454E7DDCAF4AB248BC8,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_Reset_m7BF49F82A5EFF7F32B3FA82AB48554E368DCF674,
	U3CTakeScreenshotU3Ed__4_System_Collections_IEnumerator_get_Current_m48CB53297F5D6E3CC52272F3E48E27F03C346A6B,
	MainMenu_ShowBackButton_m255ED4296523851CCDD84E6D431C2F4FB95298D8,
	MainMenu_GetGui_m10B78924EFA55AAB5117C14F7D3BD9E29AF3C68C,
	MainMenu_CallFBLogin_m1B0369D4D6E9E584DD0757FE1EE3614F782138EC,
	MainMenu_CallFBLoginForPublish_m3CC64106CA0E41CBF6955979D91795068EE5050A,
	MainMenu_CallFBLogout_m3B980F09EC74BF2E909272DBD335DF47672F1596,
	MainMenu_OnInitComplete_m3E577DB4E98D84D3102B6C4ABECB5EF469191359,
	MainMenu_OnHideUnity_m3977B8A15B9DD23E5E143E0D499FB66506A77606,
	MainMenu__ctor_m395A9D3C110C6F3E6A0671389C35A6B8B79F3F32,
	Pay_GetGui_mCB2EB9638D21C60BEC97A2BE50BB8C652170BC70,
	Pay_CallFBPay_m7C6E3F32EA8A70293A14F8F4F8E8D51777F31F5C,
	Pay__ctor_m022913AB693A521C412E05FE2782B5D07A6FD689,
};
static const int32_t s_InvokerIndices[619] = 
{
	2540,
	2540,
	2114,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2128,
	2128,
	2128,
	2540,
	2540,
	2186,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	2128,
	1326,
	2161,
	2161,
	2161,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2161,
	2540,
	2540,
	870,
	2540,
	2113,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2154,
	2540,
	2161,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2154,
	2154,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	4068,
	2540,
	1657,
	2128,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2128,
	2128,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	654,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2161,
	2161,
	2161,
	2540,
	2540,
	2540,
	870,
	2540,
	2113,
	2540,
	2540,
	2540,
	2540,
	1314,
	985,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2113,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	697,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	2128,
	2128,
	2540,
	2540,
	2540,
	2114,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	2128,
	2128,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2128,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2512,
	2512,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	586,
	2540,
	586,
	2540,
	2477,
	2128,
	2477,
	2128,
	2477,
	2128,
	2477,
	2128,
	2477,
	2128,
	2540,
	2540,
	2128,
	2128,
	1124,
	1124,
	819,
	819,
	828,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	1657,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2114,
	2540,
	2540,
	2540,
	1124,
	1124,
	819,
	819,
	828,
	2540,
	2540,
	2540,
	2540,
	2128,
	2128,
	2540,
	2540,
	2540,
	2540,
	2128,
	2540,
	2128,
	2128,
	2128,
	2128,
	2114,
	2540,
	2540,
	2540,
	2540,
	2114,
	2540,
	2540,
	2114,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2128,
	1657,
	1657,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2477,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2540,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2128,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2128,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2128,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	2540,
	2540,
	2128,
	2477,
	2540,
	2540,
	912,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2540,
	2540,
	1657,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	4185,
	4185,
	4185,
	4185,
	4191,
	4149,
	2477,
	2128,
	2477,
	2128,
	2477,
	2128,
	2533,
	2186,
	2512,
	2462,
	2477,
	2477,
	2477,
	2477,
	2540,
	1854,
	1297,
	2505,
	2128,
	2540,
	2540,
	4217,
	4149,
	2540,
	2540,
	4217,
	2540,
	2505,
	2505,
	2128,
	2540,
	2540,
	2540,
	2540,
	2540,
	2114,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2540,
	2401,
	2540,
	2505,
	2540,
	2540,
	2540,
	2128,
	2477,
	2540,
	2114,
	2540,
	2505,
	2477,
	2540,
	2477,
	2505,
	2540,
	2540,
	2540,
	2540,
	2540,
	2154,
	2540,
	2540,
	2540,
	2540,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	619,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
