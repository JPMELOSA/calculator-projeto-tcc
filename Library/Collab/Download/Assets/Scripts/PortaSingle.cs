﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortaSingle : MonoBehaviour
{
    // Start is called before the first frame update
    RespawManager _respawManager;

    float portaSpeed = 30f;
    void Start()
    {
        _respawManager = GameObject.Find("RespawManager").GetComponent<RespawManager>();
        //CollisionDetectionMode.Continuous
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0, 0, -portaSpeed * Time.deltaTime));
        limite();
        gameOver();
    }

    private void limite()
    {
        if (transform.position.z < -159f)
        {
            Destroy(this.gameObject);
        }
    }

    private void gameOver()
    {
        if (_respawManager.gameover)
        {
            Destroy(this.gameObject);
        }
    }
}
