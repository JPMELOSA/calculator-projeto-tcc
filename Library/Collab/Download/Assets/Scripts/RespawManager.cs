﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawManager : MonoBehaviour
{
    [SerializeField]
    private GameObject PortaGrandePrefab;

    [SerializeField]
    private GameObject PlayerPrefab;

    [SerializeField]
    public bool gameover = true;   

    Calculadora _calculadora;

    [SerializeField]
    public GameObject MovManager;

    [SerializeField]
    public char operacao;

    public bool operacaoEscolhida = false;

    void Start()
    {

    }

    void Update()
    {
        if (gameover)
        {
            colocaOperacao();
            if (operacaoEscolhida)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    inicio();
                }
            } 
        } 
    }

    public void InstaciarPortas() 
    {
        Instantiate(PortaGrandePrefab, new Vector3(-83.5f, -9.9f, 432.6f), Quaternion.identity);
        _calculadora.executaOperacao(operacao);
    }

    public void inicio()
    {
        MovManager.SetActive(true);
        Instantiate(PlayerPrefab, new Vector3(0, 10.02f, 14f), Quaternion.identity);
        _calculadora = GameObject.Find("Calculadora").GetComponent<Calculadora>();
        gameover = false;
        InstaciarPortas();
    }

    private void colocaOperacao()
    {
        if (Input.GetKeyDown(KeyCode.KeypadMultiply) && operacaoEscolhida == false)
        {
            operacao = '*';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus) && operacaoEscolhida == false)
        {
            operacao = '-';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadDivide) && operacaoEscolhida == false)
        {
            operacao = '/';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadPlus) && operacaoEscolhida == false)
        {
            operacao = '+';
            operacaoEscolhida = true;
        }
    }
}
