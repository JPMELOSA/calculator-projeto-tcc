﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculadora : MonoBehaviour
{
    // Start is called before the first frame update

    UIManager _uiManager;
    Porta _porta;

    int respostaCerta;

    int numeroSelecionado;
    void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _porta = GameObject.Find("PortaManager").GetComponent<Porta>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void operacao(int numero1, int numero2, char operador)
    {
        if (operador == '+')
        {
            respostaCerta = numero1 + numero2;
            _uiManager.mostraAConta.text = numero1 + " + " + numero2;
        }
        else if (operador == '-')
        {
            respostaCerta = numero1 - numero2;
            _uiManager.mostraAConta.text = numero1 + " - " + numero2;
        }
        else if (operador == '*')
        {
            respostaCerta = numero1 * numero2;
            _uiManager.mostraAConta.text = numero1 + " * " + numero2;
        }
        else if (operador == '/')
        {
            respostaCerta = numero1 / numero2;
            _uiManager.mostraAConta.text = numero1 + " / " + numero2;
            // A divisão vai ser necessário fazer os devidos tratamentos
        }
    }

    //sortear um numero de 1 a 3
    // Enviar a resposta certa para o numero sorteado e a resposta errada para outros
    private void sortearPorta()
    {
        numeroSelecionado = Random.Range(1, 4); // vai sortear de 1 a 3
        int respostaErrada1 = respostaCerta - 1; // - random.range(1, 5)
        int respostaErrada2 = respostaCerta + 1;

        if (numeroSelecionado == 1)
        {
            // Porta equerda
            _uiManager.textoEsquerdo.text = respostaCerta.ToString();
            _uiManager.textoDireito.text = respostaErrada1.ToString();
            _uiManager.textoCentro.text = respostaErrada2.ToString();
            respawPortas();
        }
        if(numeroSelecionado == 2)
        {
            // Porta central
            _uiManager.textoCentro.text = respostaCerta.ToString();
            _uiManager.textoEsquerdo.text = respostaErrada1.ToString();
            _uiManager.textoDireito.text = respostaErrada2.ToString();
            respawPortas();
        }
        if (numeroSelecionado == 3)
        {
            // Porta Direita
            _uiManager.textoDireito.text = respostaCerta.ToString();
            _uiManager.textoCentro.text = respostaErrada1.ToString();
            _uiManager.textoEsquerdo.text = respostaErrada2.ToString();
            respawPortas();
        }
    }

    // função para sortear os numero1 e numero2 e chamar a as outras funções
    public void executaOperacao()
    {
        int numero1 = Random.Range(0, 10);
        int numero2 = Random.Range(0, 10);
        operacao(numero1, numero2, '*');
        sortearPorta();
    }

    public void respawPortas()
    {
        if(numeroSelecionado == 1)
        {
            //esquerda
            Instantiate(_porta.portaCentral, new Vector3(0.4f, 30f, 420f), Quaternion.identity);
            Instantiate(_porta.portaDireita, new Vector3(38.7f, 30f, 420f), Quaternion.identity);
        }
        if(numeroSelecionado == 2)
        {
            Instantiate(_porta.portaEsquerda, new Vector3(-37.6f, 30f, 420f), Quaternion.identity);
            //centro
            Instantiate(_porta.portaDireita, new Vector3(38.7f, 30f, 420f), Quaternion.identity);
        }
        if(numeroSelecionado == 3)
        {
            Instantiate(_porta.portaEsquerda, new Vector3(-37.6f, 30f, 420f), Quaternion.identity);
            Instantiate(_porta.portaCentral, new Vector3(0.4f, 30f, 420f), Quaternion.identity);
            //direita
        }
    }
}
