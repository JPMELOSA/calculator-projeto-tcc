﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; // para armazenar o score 

public class Player : MonoBehaviour
{
    public char posicaoAtual;

    RespawManager _respawManager;

    UIManager _uiManager;

    TabeladeErros _tabeladeErros;

    Calculadora _calculadora;

    Record _record;

    MovementButton _movL;
    MovementButton _movR;

    private AudioSource _audioSource;

    private bool pararMovimento;

    public int score;

    [SerializeField]
    private GameObject teleport;

    private bool bRecord = false;
    private bool tocouTrompete = false;
    void Start()
    {
        _respawManager = GameObject.Find("RespawManager").GetComponent<RespawManager>();
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _tabeladeErros = GameObject.Find("Tabela").GetComponent<TabeladeErros>(); // Tabela de Erros
        _calculadora = GameObject.Find("Calculadora").GetComponent<Calculadora>(); // Calculadora
        _audioSource = GetComponent<AudioSource>();
        _record = GameObject.Find("Record").GetComponent<Record>();
        _movL = GameObject.Find("ButtonLeft").GetComponent<MovementButton>();
        _movR = GameObject.Find("ButtonRight").GetComponent<MovementButton>();
        posicaoAtual = 'c';
        _uiManager.mudaCor(posicaoAtual);
        pararMovimento = false;
    }
    void Update()
    {
        if (!pararMovimento)
        {
            movimento();
        }
        playerDown();
    }
    
    public void movimento()
    {
        if (Input.GetKeyDown(KeyCode.A) || _movL.leftButtonClick == true)
        {
            //Esquerda
            if (posicaoAtual == 'c')
            {
                posicaoAtual = 'e';
                //transform.position += new Vector3(-40.00000f, 0, 14f) * 10f * Time.deltaTime;
                transform.position = new Vector3(-40.00000f, 0, 14f);
                GameObject teleporte = Instantiate(teleport, new Vector3(0, 15f, 14f), Quaternion.identity);
                Destroy(teleporte, 1.0f);
                _uiManager.mudaCor(posicaoAtual);
            }
            if(posicaoAtual == 'd')
            {
                posicaoAtual = 'c';
                transform.position = new Vector3(0, 0, 14f);
                GameObject teleporte = Instantiate(teleport, new Vector3(40.00000f, 15f, 14f), Quaternion.identity);
                Destroy(teleporte, 1.0f);
                _uiManager.mudaCor(posicaoAtual);
            }
            _movL.leftButtonClick = false;
        }
        if (Input.GetKeyDown(KeyCode.D) || _movR.rightButtonClick == true)
        {
            //Direita
            if (posicaoAtual == 'c')
            {
                posicaoAtual = 'd';
                transform.position = new Vector3(40.00000f, 0, 14f);
                GameObject teleporte = Instantiate(teleport, new Vector3(0, 15f, 14f), Quaternion.identity);
                Destroy(teleporte, 1.0f);
                _uiManager.mudaCor(posicaoAtual);
            }
            if(posicaoAtual == 'e')
            {
                posicaoAtual = 'c';
                transform.position = new Vector3(0, 0, 14f); // Y = -8.990013f
                GameObject teleporte = Instantiate(teleport, new Vector3(-40.00000f, 15f, 14f), Quaternion.identity);
                Destroy(teleporte, 1.0f);
                _uiManager.mudaCor(posicaoAtual);
            }
            _movR.rightButtonClick = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GrandePorta")
        {
            pararMovimento = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "GrandePorta")
        {
            pararMovimento = false;
            score = score + 100;
            _uiManager.scoreText.text = "Score: " + score.ToString();
            _record.atualizaRecords();
            if (score > _record.scoreMaximo)
            {
                _record.scoreMaximo = score;
                if (_respawManager.operacao == '+')
                {
                    PlayerPrefs.SetInt("RecordAdicao", _record.scoreMaximo); 
                }
                if (_respawManager.operacao == '-')
                {
                    PlayerPrefs.SetInt("RecordSubtracao", _record.scoreMaximo);
                }
                if (_respawManager.operacao == '*')
                {
                    PlayerPrefs.SetInt("RecordMultiplicacao", _record.scoreMaximo);
                }
                if (_respawManager.operacao == '/')
                {
                    PlayerPrefs.SetInt("RecordDivisao", _record.scoreMaximo);
                }

                if (!tocouTrompete)
                {
                    _record._audioSourceRecord.Play();
                    tocouTrompete = true;
                }
                else
                {
                    _audioSource.Play();
                }
                _record.atualizaRecords();
                //Instantiate(_record.fogos, new Vector3(673f, -5.0f, 915f), Quaternion.identity);
            }
            else
            {
                _audioSource.Play();
            }
            _respawManager.InstaciarPortas();
        }
    }

    private void playerDown()
    {
        if(transform.position.y < -70f)
        {
            Destroy(this.gameObject);
            _respawManager.gameover = true;
            _respawManager.operacaoEscolhida = false;
            _uiManager.limpaPainel();
            _uiManager.buttonManager.SetActive(true);
            score = 0; 
            _respawManager.MovManager.SetActive(false);
            _record.scoreMaximo = 0;
            _tabeladeErros.gravaErro(_calculadora.numero1, _calculadora.numero2, _respawManager.operacao);
            _tabeladeErros.qntMulti(_respawManager.operacao);
        }
    }
}
