﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class playerScript : MonoBehaviour
{
    public GameObject GameMainController;  

    private int controlaMovimentacao = 0; // 0 esquerda / 1 meio / 2 direita
    public float SWIPE_THRESHOLD = 2f;
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    public bool detectSwipeOnlyAfterRelease = false;
    
    int i = 0;
    int ED = 1;

    // Use this for initialization

    void Start()
    {
        GameMainController = GameObject.FindGameObjectWithTag("tagdogamemaincontroler");
    }

    // Update is called once per frame
    void Update()
    {
        movimento();
        verifica_movimentacao();
    }
    
    void movimento()
    {
        if (ED == 2)
        {
            if (i != controlaMovimentacao)
            {
                transform.position += new Vector3(8, 0, 0) * 1f * Time.deltaTime;
                i++;
            }
            else ED = 1;
        }
        if (ED == 0)
        {
            if (i != controlaMovimentacao)
            {
                transform.position += new Vector3(-8, 0, 0) * 1f * Time.deltaTime;
                i--;
            }
            else ED = 1;
        }
        if (ED == 1)
        {
            if (i == -62)
            {
                transform.position = new Vector3(-3, 3, -478);
            }
            else if (i == 0)
            {
                transform.position = new Vector3(0, 3, -489);
            }
            else if (i == 62)
            {
                transform.position = new Vector3(3, 3, -478);
            }
        }
    }
    

    void verifica_movimentacao()
    {
        Touch touch = Input.touches[0];
        if (Input.touchCount == 1)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
            }

            // Detects Swipe while finger is still moving
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeOnlyAfterRelease)
                {
                    fingerDown = touch.position;
                    checkSwipe();
                }
            }

            //Detects swipe after finger is released
            if (touch.phase == TouchPhase.Ended)
            {
                detectSwipeOnlyAfterRelease = false;
            }
        }
    }

    void checkSwipe()
    {
        //Check if Horizontal swipe
        if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
        {
            if (fingerDown.x - fingerUp.x > 0)//Right swipe
            {
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                detectSwipeOnlyAfterRelease = true;
                if (controlaMovimentacao<=0)
                {
                    controlaMovimentacao += 15;
                    ED = 2;
                }
            }
            else if (fingerDown.x - fingerUp.x < 0)//Left swipe
            {
                Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                detectSwipeOnlyAfterRelease = true;
                if (controlaMovimentacao >=0)
                {
                    controlaMovimentacao -= 15;
                    ED = 0;
                }
            }
            fingerUp = fingerDown;
        }
    }

    float verticalMove()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }

    float horizontalValMove()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }



    public void resetaDirecao()
    {
        GameObject Personagem = GameObject.FindGameObjectWithTag("tagPersonagem");

        Vector3 resetaPosicao = new Vector3(0, 3, -489);

        Quaternion resetaRotacao = new Quaternion(0, 0, 0, 0);

        Personagem.transform.SetPositionAndRotation(resetaPosicao, resetaRotacao); //reseta personagem 

        ED = 0;
        controlaMovimentacao = 0;
        i = 0;
    }
}
