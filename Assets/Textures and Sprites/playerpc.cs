﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerpc : MonoBehaviour
{
    private int direcao; // 0 parado / 1 esquerda / 2 direita
    private int controlaMovimentacao = 0; 
    private int i = 0;

    public GameObject GameMainController;
    public GameObject parabens;
    public GameObject errou;
    public GameObject proximafasetext;

    // Use this for initialization
    void Start()
    {
        GameMainController = GameObject.FindGameObjectWithTag("tagdogamemaincontroler"); // bota uma tag no objeto que vai alterar, para que todo inimigo novo que nasca, ache ele
    }

    // Update is called once per frame
    void Update()
    {
        verifica_movimentacao();
        movimentacao();
    }

    private void movimentacao()
   {
        if (direcao == 1 && i != controlaMovimentacao)
        {
            i--;
            transform.position += new Vector3(-6, 0, 0) * 1f * Time.deltaTime;
        }
        else if (direcao == 2 && i != controlaMovimentacao)
        {
            i++;
            transform.position += new Vector3(6, 0, 0) * 1f * Time.deltaTime;
        }
        else direcao = 0;
   }

    private void verifica_movimentacao()
    {
        if (Input.GetKeyDown(KeyCode.A) && controlaMovimentacao >= 0)
        {
            controlaMovimentacao -= 40;
            direcao = 1;
        }
        if (Input.GetKeyDown(KeyCode.D) && controlaMovimentacao <= 0)
        {
            controlaMovimentacao += 40;
            direcao = 2;
        }
    }

    public void resetaDirecao()
    {
        GameObject Personagem = GameObject.FindGameObjectWithTag("tagPersonagem");

        Vector3 resetaPosicao = new Vector3(0, 3, -489);

        Quaternion resetaRotacao = new Quaternion(0, 0, 0, 0);

        Personagem.transform.SetPositionAndRotation(resetaPosicao, resetaRotacao); //reseta personagem 

        direcao = 0;
        controlaMovimentacao = 0;
        i = 0;
    }
}
