﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // serve para poder trocar o 'Image' atráves do Canvas

public class UIManager : MonoBehaviour
{
    #region STATIC UI MANAGER
    public static UIManager UIM;

    private void Awake()
    {
        if (UIManager.UIM == null)
        {
            UIManager.UIM = this;
        }
        else
        {
            if (UIManager.UIM != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC UI MANAGER

    [Header("Mostra a Operação")]
    [SerializeField]
    public Text mostraAConta;

    [Header("Opções")]
    [SerializeField]
    public Text textoEsquerdo;

    [SerializeField]
    public Text textoCentro;

    [SerializeField]
    public Text textoDireito;

    [Header("Score")]
    [SerializeField]
    public Text scoreText;

    [Header("Buttons")]
    [SerializeField]
    public GameObject buttonManager;

    // Scores Separados
    [Header("Records")]
    [SerializeField]
    public Text scoreAdicao;

    [SerializeField]
    public Text scoreSubtracao;

    [SerializeField]
    public Text scoreMultiplicacao;

    [SerializeField]
    public Text scoreDivisao;

    void Start()
    {
        mostraAConta.text = "Iniciar";
        mostraAConta.color = Color.black;
    }

    public void mudaCor(char local)
    {
        if(local == 'c')
        {
            textoCentro.color = Color.red;
            textoEsquerdo.color = Color.black;
            textoDireito.color = Color.black;
        }
        else if(local == 'e')
        {
            textoEsquerdo.color = Color.red;
            textoCentro.color = Color.black;
            textoDireito.color = Color.black;
        }
        else if(local == 'd')
        {
            textoDireito.color = Color.red;
            textoCentro.color = Color.black;
            textoEsquerdo.color = Color.black;
        }
    }

    public void limpaPainel()
    {
        textoCentro.color = Color.black;
        textoDireito.color = Color.black;
        textoEsquerdo.color = Color.black;
        // --
        mostraAConta.text = "Game Over";
        textoCentro.text = "";
        textoDireito.text = "";
        textoEsquerdo.text = "";
        scoreText.text = "Score: 0";
    }
}
