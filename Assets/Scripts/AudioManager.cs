using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region STATIC AUDIO MANAGER
    public static AudioManager AM;

    private void Awake()
    {
        if (AudioManager.AM == null)
        {
            AudioManager.AM = this;
        }
        else
        {
            if (AudioManager.AM != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion STATIC AUDIO MANAGER


    [Header("AUDIO SOURCES")]
    [SerializeField] private AudioSource music;
    [SerializeField] private AudioSource sfx;
    [SerializeField] private AudioSource rochaAudioSource;

    [Header("MUSICS")]
    [SerializeField] private AudioClip musicMainMenu;
    [SerializeField] private AudioClip musicGamePlay;

    [Header("SFX")]
    [SerializeField] private AudioClip mouseClick;
    [SerializeField] private AudioClip winBip;
    [SerializeField] private AudioClip loseBip;
    [SerializeField] private AudioClip transition;
    [SerializeField] private AudioClip pause;
    [SerializeField] private AudioClip applause;

    [Header("Rocha Sound")]
    [SerializeField] private AudioClip rocha;

    [Header("Volume EFX")]
    [Range(0, 1)]
    public float EFXVolume;

    [Header("Volume EFX 2")]
    [Range(0, 0.5f)]
    public float EFXVolume2;

    [Header("Volume Music")] // colocar no Volume
    [Range(0, 1)]
    public float MusicVolume;

    public void PlayMusic(string musicType)
    {
        switch (musicType)
        {
            case "MAINMENU":
                {
                    music.clip = musicMainMenu;
                    music.Play();
                    break;
                }
            case "GAMEPLAY":
                {
                    music.clip = musicGamePlay;
                    music.Play();
                    break;
                }
            case "STOP":
                {
                    music.Stop();
                    break;
                }                            
        }
        music.volume = MusicVolume;
    }

    public void PlaySFX(string SFXType)
    {
        switch (SFXType)
        {
            case "MOUSECLICK":
                {
                    sfx.PlayOneShot(mouseClick, EFXVolume);
                    break;
                }
            case "WINBIP":
                {
                    sfx.PlayOneShot(winBip, EFXVolume);
                    break;
                }
            case "LOSEBIP":
                {
                    sfx.PlayOneShot(loseBip, EFXVolume);
                    break;
                }
            case "TRANSITION":
                {
                    sfx.PlayOneShot(transition, EFXVolume);
                    break;
                }
            case "PAUSE":
                {
                    sfx.PlayOneShot(pause, EFXVolume);
                    break;
                }
            case "APPLAUSE":
                {
                    sfx.PlayOneShot(applause, 0.2f);
                    break;
                }
        }
    }

    public void PlayAudioFromTheRocks(bool Play, string text)
    {
        rochaAudioSource.clip = rocha;
        rochaAudioSource.volume = EFXVolume2;
        Debug.Log("Audio Manager: " + text);
        if (Play)
        {
            rochaAudioSource.Play();
        }
        else
        {
            rochaAudioSource.Stop();
        }
    }

    #region Configuration

    public void _EFX1Ajust(float newVolume)
    {
        EFXVolume = newVolume;
    }

    public void _EFX2Ajust(float newVolume)
    {
        EFXVolume2 = newVolume;
        rochaAudioSource.volume = EFXVolume2;
    }

    public void _MUSICAjust(float newVolume)
    {
        MusicVolume = newVolume;
        music.volume = MusicVolume;
    }

    public void StartAudioLoad()
    {
        music.volume = MusicVolume;
        rochaAudioSource.volume = EFXVolume2;
    }

    public void SaveAudioSetup()
    {
        PlayerPrefs.SetFloat("EFXVOLUME", EFXVolume);
        PlayerPrefs.SetFloat("EFXVOLUME2", EFXVolume2);
        PlayerPrefs.SetFloat("MUSICVOLUME", MusicVolume);
    }

    public void LoadAudioSetup()
    {
        if (PlayerPrefs.HasKey("EFXVOLUME"))
        {
            EFXVolume = PlayerPrefs.GetFloat("EFXVOLUME");
        }
        else
        {
            PlayerPrefs.SetFloat("EFXVOLUME", EFXVolume);
        }
        //
        if (PlayerPrefs.HasKey("EFXVOLUME2"))
        {
            EFXVolume2 = PlayerPrefs.GetFloat("EFXVOLUME2");
        }
        else
        {
            PlayerPrefs.SetFloat("EFXVOLUME2", EFXVolume2);
        }
        //
        if (PlayerPrefs.HasKey("MUSICVOLUME"))
        {
            MusicVolume = PlayerPrefs.GetFloat("MUSICVOLUME");
        }
        else
        {
            PlayerPrefs.SetFloat("MUSICVOLUME", MusicVolume);
        }
        StartAudioLoad();
    }
    #endregion Configuration
}
