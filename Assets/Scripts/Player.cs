﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; // para armazenar o score 
using DG.Tweening;

public class Player : MonoBehaviour
{
    public char posicaoAtual;

    private bool pararMovimento;

    public int score;

    private bool tocouTrompete = false;

    private bool perdeu;

    [SerializeField] private float velocidadeTransicao; // Talvez fazer um poder para acelerar a velocidade de transição
    [SerializeField] private Animator playerAnimator;

    void Start()
    {
        velocidadeTransicao = 1f;
        posicaoAtual = 'c';
        UIManager.UIM.mudaCor(posicaoAtual);
        pararMovimento = false;
    }
    void Update()
    {
        if (!pararMovimento)
        {
            movimento();
        }
        playerDown();
    }
    
    public void movimento()
    {
        if (Input.GetKeyDown(KeyCode.A) || MovementButton.MOV.leftButtonClick == true)
        {
            //Esquerda
            AudioManager.AM.PlaySFX("TRANSITION");
            if (posicaoAtual == 'c')
            {
                posicaoAtual = 'e';
                transform.DOLocalMove(new Vector3(-40.00000f, -8.990016f, 14f), velocidadeTransicao).OnComplete(Idle); ;
                left();
                UIManager.UIM.mudaCor(posicaoAtual);
            }
            if(posicaoAtual == 'd')
            {
                posicaoAtual = 'c';
                transform.DOLocalMove(new Vector3(0, -8.990016f, 14f), velocidadeTransicao).OnComplete(Idle); ;
                left();
                UIManager.UIM.mudaCor(posicaoAtual);
            }
            MovementButton.MOV.leftButtonClick = false;
        }
        if (Input.GetKeyDown(KeyCode.D) || MovementButton.MOV.rightButtonClick == true)
        {
            //Direita
            AudioManager.AM.PlaySFX("TRANSITION");
            if (posicaoAtual == 'c')
            {
                posicaoAtual = 'd';
                transform.DOLocalMove(new Vector3(40.00000f, -8.990016f, 14f), velocidadeTransicao).OnComplete(Idle);
                right();
                UIManager.UIM.mudaCor(posicaoAtual);
            }
            if(posicaoAtual == 'e')
            {
                posicaoAtual = 'c';
                transform.DOLocalMove(new Vector3(0, -8.990016f, 14f), velocidadeTransicao).OnComplete(Idle);
                right();
                UIManager.UIM.mudaCor(posicaoAtual);
            }
            MovementButton.MOV.rightButtonClick = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "GrandePorta")
        {
            pararMovimento = true;
            fall();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "GrandePorta")
        {
            Celebrate();
            pararMovimento = false;
            score = score + 100;
            UIManager.UIM.scoreText.text = "Score: " + score.ToString();
            Record.RC.atualizaRecords();
            if (score > Record.RC.scoreMaximo)
            {
                Record.RC.scoreMaximo = score;
                if (RespawManager.RM.operacao == '+')
                {
                    PlayerPrefs.SetInt("RecordAdicao", Record.RC.scoreMaximo); 
                }
                if (RespawManager.RM.operacao == '-')
                {
                    PlayerPrefs.SetInt("RecordSubtracao", Record.RC.scoreMaximo);
                }
                if (RespawManager.RM.operacao == '*')
                {
                    PlayerPrefs.SetInt("RecordMultiplicacao", Record.RC.scoreMaximo);
                }
                if (RespawManager.RM.operacao == '/')
                {
                    PlayerPrefs.SetInt("RecordDivisao", Record.RC.scoreMaximo);
                }

                if (!tocouTrompete) // QUEBROU RECORD
                {
                    AudioManager.AM.PlaySFX("APPLAUSE");
                    Record.RC._audioSourceRecord.Play();
                    RespawManager.RM.SetFireworks();
                    tocouTrompete = true;
                }
                else
                {
                    AudioManager.AM.PlaySFX("WINBIP");
                }
                Record.RC.atualizaRecords();
            }
            else
            {
                AudioManager.AM.PlaySFX("WINBIP");
            }
            RespawManager.RM.InstaciarPortas();
            AumentaVelocidadePorta();
        }
    }

    private void playerDown()
    {
        if(transform.position.y <= -14f)
        {
            if (!perdeu)
            {
                AudioManager.AM.PlaySFX("LOSEBIP");
                perdeu = true;
            }
        }
        if (transform.position.y < -50f)
        {
            Destroy(this.gameObject);
            AudioManager.AM.PlayAudioFromTheRocks(false, "Player Game Over");
            RespawManager.RM.gameover = true;
            RespawManager.RM.operacaoEscolhida = false;
            UIManager.UIM.limpaPainel();
            UIManager.UIM.buttonManager.SetActive(true);
            MenuSceneManager.MSM.ButtonsAfterGameOver();
            MenuSceneManager.MSM.pausePlay.SetActive(false);
            score = 0;
            Record.RC.scoreMaximo = 0;
            TabeladeErros.TE.gravaErro(Calculadora.CA.numero1, Calculadora.CA.numero2, RespawManager.RM.operacao);
            TabeladeErros.TE.qntMulti(RespawManager.RM.operacao);
            AumentaVelocidade.VE.velocidadeGeral = 500;
        }
    }

    private void AumentaVelocidadePorta()
    {
        AumentaVelocidade.VE.recebeVelocidade(100f);
    }

    #region Animation
    private void Idle()
    {
        playerAnimator.SetBool("Right", false);
        playerAnimator.SetBool("Left", false);
        playerAnimator.SetBool("Fall", false);
        playerAnimator.SetBool("Vitoria", false);
    }

    private void left()
    {
        playerAnimator.SetBool("Right", false);
        playerAnimator.SetBool("Left", true);
        playerAnimator.SetBool("Fall", false);
        playerAnimator.SetBool("Vitoria", false);
    }

    private void right()
    {
        playerAnimator.SetBool("Right", true);
        playerAnimator.SetBool("Left", false);
        playerAnimator.SetBool("Fall", false);
        playerAnimator.SetBool("Vitoria", false);
    }

    private void Celebrate()
    {
        playerAnimator.SetBool("Vitoria", true);
        playerAnimator.SetBool("Fall", false);
        playerAnimator.SetBool("Right", false);
        playerAnimator.SetBool("Left", false);
        StartCoroutine(stopCelebrating());
    }

    private void fall()
    {
        playerAnimator.SetBool("Fall", true);
        playerAnimator.SetBool("Right", false);
        playerAnimator.SetBool("Left", false);
        playerAnimator.SetBool("Vitoria", false);
    }

    IEnumerator stopCelebrating()
    {
        yield return new WaitForSeconds(2);
        Idle();
    }
    #endregion Animation
}
