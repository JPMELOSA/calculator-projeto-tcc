﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour
{
    private float transitionValue;
    void Start()
    {
        transitionValue = 1;
        iniciaAudioMaster();
    }

    public void start()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("03-GamePlay", 1);
    }

    public void aprender()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("07-Aprender", transitionValue);
    }

    public void ranking()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("Ranking", transitionValue);
    }

    public void ajuda()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("06-Ajuda", transitionValue);
    }

    public void sobre()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("05-Sobre", transitionValue);
    }

    public void config()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("04-Config", transitionValue);
    }

    public void returnMenu()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        AudioManager.AM.PlayAudioFromTheRocks(false, "FALSE - Retorno Menu");
        AudioManager.AM.SaveAudioSetup();
        TransitionSceneManager.TSC.ChangeScene("02-Menu", transitionValue);
    }

    // Menu Aprender 
    public void adicao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("08-Adicao", transitionValue);
    }
    public void subtracao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("09-Subtracao", transitionValue);
    }
    public void multiplicacao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("10-Multiplicacao", transitionValue);
    }
    public void divisao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("11-Divisao", transitionValue);
    }

    public void tabelaErros()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("12-Tabela", transitionValue);
    }

    public void iniciaAudioMaster()
    {
        if (PlayerPrefs.HasKey("audioMaster"))
        {
            string somSalvo = PlayerPrefs.GetString("audioMaster");

            if (somSalvo == "Desligado")
            {
                AudioListener.volume = 0;
            }
            if (somSalvo == "Ligado")
            {
                AudioListener.volume = 1;
            }
        }
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
