﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Record : MonoBehaviour
{
    #region STATIC RECORD
    public static Record RC;

    private void Awake()
    {
        if (Record.RC == null)
        {
            Record.RC = this;
        }
        else
        {
            if (Record.RC != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC RECORD

    [SerializeField]
    public int scoreMaximo;

    public AudioSource _audioSourceRecord;

    // Records
    [SerializeField]
    private int scoreMaximoAdicao;

    [SerializeField]
    private int scoreMaximoSubtracao;

    [SerializeField]
    private int scoreMaximoMultiplicacao;

    [SerializeField]
    private int scoreMaximoDivisao;

    // Fogos de artifício
    [SerializeField]
    public GameObject fogos;

    void Start()
    {
        _audioSourceRecord = GetComponent<AudioSource>();
        atualizaRecords();
    }

    public void atualizaRecords()
    {
        receberRecordsGravados(); 
        if (RespawManager.RM.operacao == '+')
        {
            UIManager.UIM.scoreAdicao.text = "Adição: " + scoreMaximoAdicao;
            if(scoreMaximoAdicao > scoreMaximo)
            {
                scoreMaximo = scoreMaximoAdicao;
            }
        }
        if (RespawManager.RM.operacao == '-')
        {
            UIManager.UIM.scoreSubtracao.text = "Subtração: " + scoreMaximoSubtracao;
            if(scoreMaximoSubtracao > scoreMaximo)
            {
                scoreMaximo = scoreMaximoSubtracao;
            } 
        }
        if (RespawManager.RM.operacao == '*')
        {
            UIManager.UIM.scoreMultiplicacao.text = "Multiplicação: " + scoreMaximoMultiplicacao;
            if(scoreMaximoMultiplicacao > scoreMaximo)
            {
                scoreMaximo = scoreMaximoMultiplicacao;
            } 
        }
        if (RespawManager.RM.operacao == '/')
        {
            UIManager.UIM.scoreDivisao.text = "Divisão: " + scoreMaximoDivisao;
            if(scoreMaximoDivisao > scoreMaximo)
            {
                scoreMaximo = scoreMaximoDivisao;
            }   
        }
    }

    private void receberRecordsGravados()
    {
        if (PlayerPrefs.HasKey("RecordAdicao"))
        {
            scoreMaximoAdicao = PlayerPrefs.GetInt("RecordAdicao");
            UIManager.UIM.scoreAdicao.text = "Adição: " + scoreMaximoAdicao;
        }
        if (PlayerPrefs.HasKey("RecordSubtracao"))
        {
            scoreMaximoSubtracao = PlayerPrefs.GetInt("RecordSubtracao");
            UIManager.UIM.scoreSubtracao.text = "Subtração: " + scoreMaximoSubtracao;
        }
        if (PlayerPrefs.HasKey("RecordMultiplicacao"))
        {
            scoreMaximoMultiplicacao = PlayerPrefs.GetInt("RecordMultiplicacao");
            UIManager.UIM.scoreMultiplicacao.text = "Multiplicação: " + scoreMaximoMultiplicacao;
        }
        if (PlayerPrefs.HasKey("RecordDivisao"))
        {
            scoreMaximoDivisao = PlayerPrefs.GetInt("RecordDivisao");
            UIManager.UIM.scoreDivisao.text = "Divisão: " + scoreMaximoDivisao;
        }
    }
}
