﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private GameObject soundOn;
    [SerializeField]
    private GameObject soundOff;
    [SerializeField]
    private GameObject musicOn;
    [SerializeField]
    private GameObject musicOff;

    // Audio

    // Musica

    private bool sound = true;

    private bool music = true;

    private string somSalvo;

    private float volumeMaster;
    void Start()
    {
        iniciaAudioMaster();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void soundOnOff()
    {
        if(soundOn != null && soundOff != null)
        {
            sound = !sound;
            soundOn.SetActive(sound);
            soundOff.SetActive(!sound);
            desligaSom(sound);
        }
    }

    public void musicOnOff()
    {
        music = !music;
        musicOn.SetActive(music);
        musicOff.SetActive(!music);
    }

    private void desligaSom(bool som)
    {
        if (som)
        {
            AudioListener.volume = 1;
            PlayerPrefs.SetString("audioMaster", "Ligado"); 
        }
        else
        {
            AudioListener.volume = 0;
            PlayerPrefs.SetString("audioMaster", "Desligado");
        }
    }

    public void iniciaAudioMaster()
    {
        if (PlayerPrefs.HasKey("audioMaster"))
        {
            somSalvo = PlayerPrefs.GetString("audioMaster");
        }

        if (somSalvo == "Desligado")
        {
            if (soundOn != null && soundOff != null)
            {
                soundOn.SetActive(false);
                soundOff.SetActive(true);
                AudioListener.volume = 0;
                sound = false;
            }   
        }
        if (somSalvo == "Ligado")
        {
            if (soundOn != null && soundOff != null)
            {
                soundOn.SetActive(true);
                soundOff.SetActive(false);
                AudioListener.volume = 1;
                sound = true;
            }   
        }
    }

    public void VolumeMaster(float volume)
    {
        volumeMaster = volume;
        AudioListener.volume = volumeMaster;
    }
}
