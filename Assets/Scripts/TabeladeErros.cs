﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class TabeladeErros : MonoBehaviour
{
    #region STATIC TABELA DE ERROS
    public static TabeladeErros TE;

    private void Awake()
    {
        if (TabeladeErros.TE == null)
        {
            TabeladeErros.TE = this;
        }
        else
        {
            if (TabeladeErros.TE != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC TABELA DE ERROS

    #region TABUADA
    #region 1
    [SerializeField]
    private TextMeshProUGUI erro1x0;
    [SerializeField]
    private TextMeshProUGUI erro1x1;
    [SerializeField]
    private TextMeshProUGUI erro1x2;
    [SerializeField]
    private TextMeshProUGUI erro1x3;
    [SerializeField]
    private TextMeshProUGUI erro1x4;
    [SerializeField]
    private TextMeshProUGUI erro1x5;
    [SerializeField]
    private TextMeshProUGUI erro1x6;
    [SerializeField]
    private TextMeshProUGUI erro1x7;
    [SerializeField]
    private TextMeshProUGUI erro1x8;
    [SerializeField]
    private TextMeshProUGUI erro1x9;
    [SerializeField]
    private TextMeshProUGUI erro1x10;
    #endregion 1
    // ---------------------------------------
    #region 2
    [SerializeField]
    private TextMeshProUGUI erro2x0;
    [SerializeField]
    private TextMeshProUGUI erro2x1;
    [SerializeField]
    private TextMeshProUGUI erro2x2;
    [SerializeField]
    private TextMeshProUGUI erro2x3;
    [SerializeField]
    private TextMeshProUGUI erro2x4;
    [SerializeField]
    private TextMeshProUGUI erro2x5;
    [SerializeField]
    private TextMeshProUGUI erro2x6;
    [SerializeField]
    private TextMeshProUGUI erro2x7;
    [SerializeField]
    private TextMeshProUGUI erro2x8;
    [SerializeField]
    private TextMeshProUGUI erro2x9;
    [SerializeField]
    private TextMeshProUGUI erro2x10;
    #endregion 2
    // ---------------------------------------
    #region 3
    [SerializeField]
    private TextMeshProUGUI erro3x0;
    [SerializeField]
    private TextMeshProUGUI erro3x1;
    [SerializeField]
    private TextMeshProUGUI erro3x2;
    [SerializeField]
    private TextMeshProUGUI erro3x3;
    [SerializeField]
    private TextMeshProUGUI erro3x4;
    [SerializeField]
    private TextMeshProUGUI erro3x5;
    [SerializeField]
    private TextMeshProUGUI erro3x6;
    [SerializeField]
    private TextMeshProUGUI erro3x7;
    [SerializeField]
    private TextMeshProUGUI erro3x8;
    [SerializeField]
    private TextMeshProUGUI erro3x9;
    [SerializeField]
    private TextMeshProUGUI erro3x10;
    #endregion 3
    // --------------------------------------
    #region 4
    [SerializeField]
    private TextMeshProUGUI erro4x0;
    [SerializeField]
    private TextMeshProUGUI erro4x1;
    [SerializeField]
    private TextMeshProUGUI erro4x2;
    [SerializeField]
    private TextMeshProUGUI erro4x3;
    [SerializeField]
    private TextMeshProUGUI erro4x4;
    [SerializeField]
    private TextMeshProUGUI erro4x5;
    [SerializeField]
    private TextMeshProUGUI erro4x6;
    [SerializeField]
    private TextMeshProUGUI erro4x7;
    [SerializeField]
    private TextMeshProUGUI erro4x8;
    [SerializeField]
    private TextMeshProUGUI erro4x9;
    [SerializeField]
    private TextMeshProUGUI erro4x10;
    #endregion 4
    // ---------------------------------------
    #region 5
    [SerializeField]
    private TextMeshProUGUI erro5x0;
    [SerializeField]
    private TextMeshProUGUI erro5x1;
    [SerializeField]
    private TextMeshProUGUI erro5x2;
    [SerializeField]
    private TextMeshProUGUI erro5x3;
    [SerializeField]
    private TextMeshProUGUI erro5x4;
    [SerializeField]
    private TextMeshProUGUI erro5x5;
    [SerializeField]
    private TextMeshProUGUI erro5x6;
    [SerializeField]
    private TextMeshProUGUI erro5x7;
    [SerializeField]
    private TextMeshProUGUI erro5x8;
    [SerializeField]
    private TextMeshProUGUI erro5x9;
    [SerializeField]
    private TextMeshProUGUI erro5x10;
    #endregion 5
    // ----------------------------------------
    #region 6
    [SerializeField]
    private TextMeshProUGUI erro6x0;
    [SerializeField]
    private TextMeshProUGUI erro6x1;
    [SerializeField]
    private TextMeshProUGUI erro6x2;
    [SerializeField]
    private TextMeshProUGUI erro6x3;
    [SerializeField]
    private TextMeshProUGUI erro6x4;
    [SerializeField]
    private TextMeshProUGUI erro6x5;
    [SerializeField]
    private TextMeshProUGUI erro6x6;
    [SerializeField]
    private TextMeshProUGUI erro6x7;
    [SerializeField]
    private TextMeshProUGUI erro6x8;
    [SerializeField]
    private TextMeshProUGUI erro6x9;
    [SerializeField]
    private TextMeshProUGUI erro6x10;
    #endregion 6
    // ----------------------------------------
    #region 7
    [SerializeField]
    private TextMeshProUGUI erro7x0;
    [SerializeField]
    private TextMeshProUGUI erro7x1;
    [SerializeField]
    private TextMeshProUGUI erro7x2;
    [SerializeField]
    private TextMeshProUGUI erro7x3;
    [SerializeField]
    private TextMeshProUGUI erro7x4;
    [SerializeField]
    private TextMeshProUGUI erro7x5;
    [SerializeField]
    private TextMeshProUGUI erro7x6;
    [SerializeField]
    private TextMeshProUGUI erro7x7;
    [SerializeField]
    private TextMeshProUGUI erro7x8;
    [SerializeField]
    private TextMeshProUGUI erro7x9;
    [SerializeField]
    private TextMeshProUGUI erro7x10;
    #endregion 7
    // ----------------------------------------
    #region 8
    [SerializeField]
    private TextMeshProUGUI erro8x0;
    [SerializeField]
    private TextMeshProUGUI erro8x1;
    [SerializeField]
    private TextMeshProUGUI erro8x2;
    [SerializeField]
    private TextMeshProUGUI erro8x3;
    [SerializeField]
    private TextMeshProUGUI erro8x4;
    [SerializeField]
    private TextMeshProUGUI erro8x5;
    [SerializeField]
    private TextMeshProUGUI erro8x6;
    [SerializeField]
    private TextMeshProUGUI erro8x7;
    [SerializeField]
    private TextMeshProUGUI erro8x8;
    [SerializeField]
    private TextMeshProUGUI erro8x9;
    [SerializeField]
    private TextMeshProUGUI erro8x10;
    #endregion 8
    // ----------------------------------------
    #region 9
    [SerializeField]
    private TextMeshProUGUI erro9x0;
    [SerializeField]
    private TextMeshProUGUI erro9x1;
    [SerializeField]
    private TextMeshProUGUI erro9x2;
    [SerializeField]
    private TextMeshProUGUI erro9x3;
    [SerializeField]
    private TextMeshProUGUI erro9x4;
    [SerializeField]
    private TextMeshProUGUI erro9x5;
    [SerializeField]
    private TextMeshProUGUI erro9x6;
    [SerializeField]
    private TextMeshProUGUI erro9x7;
    [SerializeField]
    private TextMeshProUGUI erro9x8;
    [SerializeField]
    private TextMeshProUGUI erro9x9;
    [SerializeField]
    private TextMeshProUGUI erro9x10;
    #endregion 9
    // ---------------------------------------
    #region 10
    [SerializeField]
    private TextMeshProUGUI erro10x0;
    [SerializeField]
    private TextMeshProUGUI erro10x1;
    [SerializeField]
    private TextMeshProUGUI erro10x2;
    [SerializeField]
    private TextMeshProUGUI erro10x3;
    [SerializeField]
    private TextMeshProUGUI erro10x4;
    [SerializeField]
    private TextMeshProUGUI erro10x5;
    [SerializeField]
    private TextMeshProUGUI erro10x6;
    [SerializeField]
    private TextMeshProUGUI erro10x7;
    [SerializeField]
    private TextMeshProUGUI erro10x8;
    [SerializeField]
    private TextMeshProUGUI erro10x9;
    [SerializeField]
    private TextMeshProUGUI erro10x10;
    #endregion 10
    #endregion TABUADA

    [SerializeField]
    private TextMeshProUGUI quantidadeMultip;

    void Start()
    {
        atualizaTabela();
        atualizaNumeroVezes();
    }

    public void gravaErro(float numero1, float numero2, char operador)
    {
        if(operador == '*')
        {
            int erro = 0;
            if (PlayerPrefs.HasKey("erro" + numero1 + "x" + numero2))
            {
                erro = PlayerPrefs.GetInt("erro" + numero1 + "x" + numero2);
            }
            else
            {
                erro = 0;
            }
            PlayerPrefs.SetInt("erro" + numero1 + "x" + numero2, erro + 1);
        }
    }

    private void atualizaTabela()
    {
        // -------------------  1 ---------------------
        if (PlayerPrefs.HasKey("erro1x0"))
        {
            if (erro1x0 != null)
            {
                erro1x0.text = PlayerPrefs.GetInt("erro1x0").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro1x1"))
        {
            if (erro1x1 != null)
            {
                erro1x1.text = PlayerPrefs.GetInt("erro1x1").ToString() + " Erro(s)";
            }      
        }
        if (PlayerPrefs.HasKey("erro1x2"))
        {
            if (erro1x2 != null)
            {
                erro1x2.text = PlayerPrefs.GetInt("erro1x2").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro1x3"))
        {
            if (erro1x3 != null)
            {
                erro1x3.text = PlayerPrefs.GetInt("erro1x3").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro1x4"))
        {
            if (erro1x4 != null)
            {
                erro1x4.text = PlayerPrefs.GetInt("erro1x4").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro1x5"))
        {
            if (erro1x5 != null)
            {
                erro1x5.text = PlayerPrefs.GetInt("erro1x5").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro1x6"))
        {
            if (erro1x6 != null)
            {
                erro1x6.text = PlayerPrefs.GetInt("erro1x6").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro1x7"))
        {
            if (erro1x7 != null)
            {
                erro1x7.text = PlayerPrefs.GetInt("erro1x7").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro1x8"))
        {
            if (erro1x8 != null)
            {
                erro1x8.text = PlayerPrefs.GetInt("erro1x8").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro1x9"))
        {
            if (erro1x9 != null)
            {
                erro1x9.text = PlayerPrefs.GetInt("erro1x9").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro1x10"))
        {
            if (erro1x10 != null)
            {
                erro1x10.text = PlayerPrefs.GetInt("erro1x10").ToString() + " Erro(s)";
            }     
        }

        // --------------------- 2 ------------------------------

        if (PlayerPrefs.HasKey("erro2x0"))
        {
            if (erro2x0 != null)
            {
                erro2x0.text = PlayerPrefs.GetInt("erro2x0").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro2x1"))
        {
            if (erro2x1 != null)
            {
                erro2x1.text = PlayerPrefs.GetInt("erro2x1").ToString() + " Erro(s)";
            }       
        }
        if (PlayerPrefs.HasKey("erro2x2"))
        {
            if (erro2x2 != null)
            {
                erro2x2.text = PlayerPrefs.GetInt("erro2x2").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro2x3"))
        {
            if (erro2x3 != null)
            {
                erro2x3.text = PlayerPrefs.GetInt("erro2x3").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro2x4"))
        {
            if (erro2x4 != null)
            {
                erro2x4.text = PlayerPrefs.GetInt("erro2x4").ToString() + " Erro(s)";
            }       
        }
        if (PlayerPrefs.HasKey("erro2x5"))
        {
            if (erro2x5 != null)
            {
                erro2x5.text = PlayerPrefs.GetInt("erro2x5").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro2x6"))
        {
            if (erro2x6 != null)
            {
                erro2x6.text = PlayerPrefs.GetInt("erro2x6").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro2x7"))
        {
            if (erro2x7 != null)
            {
                erro2x7.text = PlayerPrefs.GetInt("erro2x7").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro2x8"))
        {
            if (erro2x8 != null)
            {
                erro2x8.text = PlayerPrefs.GetInt("erro2x8").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro2x9"))
        {
            if (erro2x9 != null)
            {
                erro2x9.text = PlayerPrefs.GetInt("erro2x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro2x10"))
        {
            if (erro2x10 != null)
            {
                erro2x10.text = PlayerPrefs.GetInt("erro2x10").ToString() + " Erro(s)";
            }    
        }

        // -------------------------- 3 ----------------------------------

        if (PlayerPrefs.HasKey("erro3x0"))
        {
            if (erro3x0 != null)
            {
                erro3x0.text = PlayerPrefs.GetInt("erro3x0").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro3x1"))
        {
            if (erro3x1 != null)
            {
                erro3x1.text = PlayerPrefs.GetInt("erro3x1").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro3x2"))
        {
            if (erro3x2 != null)
            {
                erro3x2.text = PlayerPrefs.GetInt("erro3x2").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro3x3"))
        {
            if (erro3x3 != null)
            {
                erro3x3.text = PlayerPrefs.GetInt("erro3x3").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro3x4"))
        {
            if (erro3x4 != null)
            {
                erro3x4.text = PlayerPrefs.GetInt("erro3x4").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro3x5"))
        {
            if (erro3x5 != null)
            {
                erro3x5.text = PlayerPrefs.GetInt("erro3x5").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro3x6"))
        {
            if (erro3x6 != null)
            {
                erro3x6.text = PlayerPrefs.GetInt("erro3x6").ToString() + " Erro(s)";
            }      
        }
        if (PlayerPrefs.HasKey("erro3x7"))
        {
            if (erro3x7 != null)
            {
                erro3x7.text = PlayerPrefs.GetInt("erro3x7").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro3x8"))
        {
            if (erro3x8 != null)
            {
                erro3x8.text = PlayerPrefs.GetInt("erro3x8").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro3x9"))
        {
            if (erro3x9 != null)
            {
                erro3x9.text = PlayerPrefs.GetInt("erro3x9").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro3x10"))
        {
            if (erro3x10 != null)
            {
                erro3x10.text = PlayerPrefs.GetInt("erro3x10").ToString() + " Erro(s)";
            }  
        }

        // ------------------------ 4 -----------------------------------


        if (PlayerPrefs.HasKey("erro4x0"))
        {
            if (erro4x0 != null)
            {
                erro4x0.text = PlayerPrefs.GetInt("erro4x0").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro4x1"))
        {
            if (erro4x1 != null)
            {
                erro4x1.text = PlayerPrefs.GetInt("erro4x1").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro4x2"))
        {
            if (erro4x2 != null)
            {
                erro4x2.text = PlayerPrefs.GetInt("erro4x2").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro4x3"))
        {
            if (erro4x3 != null)
            {
                erro4x3.text = PlayerPrefs.GetInt("erro4x3").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro4x4"))
        {
            if (erro4x4 != null)
            {
                erro4x4.text = PlayerPrefs.GetInt("erro4x4").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro4x5"))
        {
            if (erro4x5 != null)
            {
                erro4x5.text = PlayerPrefs.GetInt("erro4x5").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro4x6"))
        {
            if (erro4x6 != null)
            {
                erro4x6.text = PlayerPrefs.GetInt("erro4x6").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro4x7"))
        {
            if (erro4x7 != null)
            {
                erro4x7.text = PlayerPrefs.GetInt("erro4x7").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro4x8"))
        {
            if (erro4x8 != null)
            {
                erro4x8.text = PlayerPrefs.GetInt("erro4x8").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro4x9"))
        {
            if (erro4x9 != null)
            {
                erro4x9.text = PlayerPrefs.GetInt("erro4x9").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro4x10"))
        {
            if (erro4x10 != null)
            {
                erro4x10.text = PlayerPrefs.GetInt("erro4x10").ToString() + " Erro(s)";
            }
        }

        // ----------------------- 5 --------------------------------

        if (PlayerPrefs.HasKey("erro5x0"))
        {
            if (erro5x0 != null)
            {
                erro5x0.text = PlayerPrefs.GetInt("erro5x0").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro5x1"))
        {
            if (erro5x1 != null)
            {
                erro5x1.text = PlayerPrefs.GetInt("erro5x1").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro5x2"))
        {
            if (erro5x2 != null)
            {
                erro5x2.text = PlayerPrefs.GetInt("erro5x2").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro5x3"))
        {
            if (erro5x3 != null)
            {
                erro5x3.text = PlayerPrefs.GetInt("erro5x3").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro5x4"))
        {
            if (erro5x4 != null)
            {
                erro5x4.text = PlayerPrefs.GetInt("erro5x4").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro5x5"))
        {
            if (erro5x5 != null)
            {
                erro5x5.text = PlayerPrefs.GetInt("erro5x5").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro5x6"))
        {
            if (erro5x6 != null)
            {
                erro5x6.text = PlayerPrefs.GetInt("erro5x6").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro5x7"))
        {
            if (erro5x7 != null)
            {
                erro5x7.text = PlayerPrefs.GetInt("erro5x7").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro5x8"))
        {
            if (erro5x8 != null)
            {
                erro5x8.text = PlayerPrefs.GetInt("erro5x8").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro5x9"))
        {
            if (erro5x9 != null)
            {
                erro5x9.text = PlayerPrefs.GetInt("erro5x9").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro5x10"))
        {
            if (erro5x10 != null)
            {
                erro5x10.text = PlayerPrefs.GetInt("erro5x10").ToString() + " Erro(s)";
            }
        }

        // ---------------------- 6 -----------------------------

        if (PlayerPrefs.HasKey("erro6x0"))
        {
            if (erro6x0 != null)
            {
                erro6x0.text = PlayerPrefs.GetInt("erro6x0").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x1"))
        {
            if (erro6x1 != null)
            {
                erro6x1.text = PlayerPrefs.GetInt("erro6x1").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x2"))
        {
            if (erro6x2 != null)
            {
                erro6x2.text = PlayerPrefs.GetInt("erro6x2").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro6x3"))
        {
            if (erro6x3 != null)
            {
                erro6x3.text = PlayerPrefs.GetInt("erro6x3").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro6x4"))
        {
            if (erro6x4 != null)
            {
                erro6x4.text = PlayerPrefs.GetInt("erro6x4").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro6x5"))
        {
            if (erro6x5 != null)
            {
                erro6x5.text = PlayerPrefs.GetInt("erro6x5").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x6"))
        {
            if (erro6x6 != null)
            {
                erro6x6.text = PlayerPrefs.GetInt("erro6x6").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x7"))
        {
            if (erro6x7 != null)
            {
                erro6x7.text = PlayerPrefs.GetInt("erro6x7").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x8"))
        {
            if (erro6x8 != null)
            {
                erro6x8.text = PlayerPrefs.GetInt("erro6x8").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x9"))
        {
            if (erro6x9 != null)
            {
                erro6x9.text = PlayerPrefs.GetInt("erro6x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro6x10"))
        {
            if (erro6x10 != null)
            {
                erro6x10.text = PlayerPrefs.GetInt("erro6x10").ToString() + " Erro(s)";
            }
        }

        // ------------------ 7 -----------------------

        if (PlayerPrefs.HasKey("erro7x0"))
        {
            if (erro7x0 != null)
            {
                erro7x0.text = PlayerPrefs.GetInt("erro7x0").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro7x1"))
        {
            if (erro7x1 != null)
            {
                erro7x1.text = PlayerPrefs.GetInt("erro7x1").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro7x2"))
        {
            if (erro7x2 != null)
            {
                erro7x2.text = PlayerPrefs.GetInt("erro7x2").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro7x3"))
        {
            if (erro7x3 != null)
            {
                erro7x3.text = PlayerPrefs.GetInt("erro7x3").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro7x4"))
        {
            if (erro7x4 != null)
            {
                erro7x4.text = PlayerPrefs.GetInt("erro7x4").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro7x5"))
        {
            if (erro7x5 != null)
            {
                erro7x5.text = PlayerPrefs.GetInt("erro7x5").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro7x6"))
        {
            if (erro7x6 != null)
            {
                erro7x6.text = PlayerPrefs.GetInt("erro7x6").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro7x7"))
        {
            if (erro7x7 != null)
            {
                erro7x7.text = PlayerPrefs.GetInt("erro7x7").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro7x8"))
        {
            if (erro7x8 != null)
            {
                erro7x8.text = PlayerPrefs.GetInt("erro7x8").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro7x9"))
        {
            if (erro7x9 != null)
            {
                erro7x9.text = PlayerPrefs.GetInt("erro7x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro7x10"))
        {
            if (erro7x10 != null)
            {
                erro7x10.text = PlayerPrefs.GetInt("erro7x10").ToString() + " Erro(s)";
            } 
        }

        // -------------- 8 ---------------

        if (PlayerPrefs.HasKey("erro8x0"))
        {
            if (erro8x0 != null)
            {
                erro8x0.text = PlayerPrefs.GetInt("erro8x0").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro8x1"))
        {
            if (erro8x1 != null)
            {
                erro8x1.text = PlayerPrefs.GetInt("erro8x1").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro8x2"))
        {
            if (erro8x2 != null)
            {
                erro8x2.text = PlayerPrefs.GetInt("erro8x2").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro8x3"))
        {
            if (erro8x3 != null)
            {
                erro8x3.text = PlayerPrefs.GetInt("erro8x3").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro8x4"))
        {
            if (erro8x4 != null)
            {
                erro8x4.text = PlayerPrefs.GetInt("erro8x4").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro8x5"))
        {
            if (erro8x5 != null)
            {
                erro8x5.text = PlayerPrefs.GetInt("erro8x5").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro8x6"))
        {
            if (erro8x6 != null)
            {
                erro8x6.text = PlayerPrefs.GetInt("erro8x6").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro8x7"))
        {
            if (erro8x7 != null)
            {
                erro8x7.text = PlayerPrefs.GetInt("erro8x7").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro8x8"))
        {
            if (erro8x8 != null)
            {
                erro8x8.text = PlayerPrefs.GetInt("erro8x8").ToString() + " Erro(s)";
            }    
        }
        if (PlayerPrefs.HasKey("erro8x9"))
        {
            if (erro8x9 != null)
            {
                erro8x9.text = PlayerPrefs.GetInt("erro8x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro8x10"))
        {
            if (erro8x10 != null)
            {
                erro8x10.text = PlayerPrefs.GetInt("erro8x10").ToString() + " Erro(s)";
            } 
        }

        // --------------- 9 ----------------------

        if (PlayerPrefs.HasKey("erro9x0"))
        {
            if (erro9x0 != null)
            {
                erro9x0.text = PlayerPrefs.GetInt("erro9x0").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro9x1"))
        {
            if (erro9x1 != null)
            {
                erro9x1.text = PlayerPrefs.GetInt("erro9x1").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro9x2"))
        {
            if (erro9x2 != null)
            {
                erro9x2.text = PlayerPrefs.GetInt("erro9x2").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro9x3"))
        {
            if (erro9x3 != null)
            {
                erro9x3.text = PlayerPrefs.GetInt("erro9x3").ToString() + " Erro(s)";
            }   
        }
        if (PlayerPrefs.HasKey("erro9x4"))
        {
            if (erro9x4 != null)
            {
                erro9x4.text = PlayerPrefs.GetInt("erro9x4").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro9x5"))
        {
            if (erro9x5 != null)
            {
                erro9x5.text = PlayerPrefs.GetInt("erro9x5").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro9x6"))
        {
            if (erro9x6 != null)
            {
                erro9x6.text = PlayerPrefs.GetInt("erro9x6").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro9x7"))
        {
            if (erro9x7 != null)
            {
                erro9x7.text = PlayerPrefs.GetInt("erro9x7").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro9x8"))
        {
            if (erro9x8 != null)
            {
                erro9x8.text = PlayerPrefs.GetInt("erro9x8").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro9x9"))
        {
            if (erro9x9 != null)
            {
                erro9x9.text = PlayerPrefs.GetInt("erro9x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro9x10"))
        {
            if (erro9x10 != null)
            {
                erro9x10.text = PlayerPrefs.GetInt("erro9x10").ToString() + " Erro(s)";
            }
        }

        // --------------- 10 -------------------

        if (PlayerPrefs.HasKey("erro10x0"))
        {
            if (erro10x0 != null)
            {
                erro10x0.text = PlayerPrefs.GetInt("erro10x0").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro10x1"))
        {
            if (erro10x1 != null)
            {
                erro10x1.text = PlayerPrefs.GetInt("erro10x1").ToString() + " Erro(s)";
            } 
        }
        if (PlayerPrefs.HasKey("erro10x2"))
        {
            if (erro10x2 != null)
            {
                erro10x2.text = PlayerPrefs.GetInt("erro10x2").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x3"))
        {
            if (erro10x3 != null)
            {
                erro10x3.text = PlayerPrefs.GetInt("erro10x3").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x4"))
        {
            if (erro10x4 != null)
            {
                erro10x4.text = PlayerPrefs.GetInt("erro10x4").ToString() + " Erro(s)";
            }  
        }
        if (PlayerPrefs.HasKey("erro10x5"))
        {
            if (erro10x5 != null)
            {
                erro10x5.text = PlayerPrefs.GetInt("erro10x5").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x6"))
        {
            if (erro10x6 != null)
            {
                erro10x6.text = PlayerPrefs.GetInt("erro10x6").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x7"))
        {
            if (erro10x7 != null)
            {
                erro10x7.text = PlayerPrefs.GetInt("erro10x7").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x8"))
        {
            if (erro10x8 != null)
            {
                erro10x8.text = PlayerPrefs.GetInt("erro10x8").ToString() + " Erro(s)";
            }     
        }
        if (PlayerPrefs.HasKey("erro10x9"))
        {
            if (erro10x9 != null)
            {
                erro10x9.text = PlayerPrefs.GetInt("erro10x9").ToString() + " Erro(s)";
            }
        }
        if (PlayerPrefs.HasKey("erro10x10"))
        {
            if (erro10x10 != null)
            {
                erro10x10.text = PlayerPrefs.GetInt("erro10x10").ToString() + " Erro(s)";
            }
        }
    }

    public void qntMulti(char operador)
    {
        if (operador == '*')
        {
            int numero = 0;
            if (PlayerPrefs.HasKey("qntMulti"))
            {
                numero = PlayerPrefs.GetInt("qntMulti");
            }
            else
            {
                numero = 0;
            }
            PlayerPrefs.SetInt("qntMulti", numero + 1);
        }
    }

    private void atualizaNumeroVezes()
    {
        if (PlayerPrefs.HasKey("qntMulti"))
        {
            if (quantidadeMultip != null)
            {
                quantidadeMultip.text = PlayerPrefs.GetInt("qntMulti").ToString();
            }
        }
    }
}
