using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Configuration : MonoBehaviour
{
    [Header("VALUES")]

    [SerializeField] private Slider sliderEFX1;

    [SerializeField] private Slider sliderEFX2;

    [SerializeField] private Slider sliderMUSIC;

    void Start()
    {
        AudioManager.AM.PlayAudioFromTheRocks(true, "Configuration");
        sliderEFX1.value = AudioManager.AM.EFXVolume;
        sliderEFX2.value = AudioManager.AM.EFXVolume2;
        sliderMUSIC.value = AudioManager.AM.MusicVolume;
        Debug.Log("START CONFIGURATION");
    }
}
