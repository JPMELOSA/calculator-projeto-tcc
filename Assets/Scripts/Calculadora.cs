﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculadora : MonoBehaviour
{
    #region STATIC CALCULADORA
    public static Calculadora CA;

    private void Awake()
    {
        if (Calculadora.CA == null)
        {
            Calculadora.CA = this;
        }
        else
        {
            if (Calculadora.CA != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC CALCULADORA


    float respostaCerta;
    int numeroSelecionado;

    // Portas
    [SerializeField]
    public GameObject portaEsquerda;

    [SerializeField]
    public GameObject portaCentral;

    [SerializeField]
    public GameObject portaDireita;

    public float numero1;

    public float numero2;

    private void operacao(float numero1, float numero2, char operador)
    {
        if (operador == '+')
        {
            respostaCerta = numero1 + numero2;
            UIManager.UIM.mostraAConta.text = numero1 + " + " + numero2;
        }
        else if (operador == '-')
        {
            if (numero1 < numero2)
            {
                numero1 = numero2 + Random.Range(1, 3);
            }

            respostaCerta = numero1 - numero2;
            UIManager.UIM.mostraAConta.text = numero1 + " - " + numero2;
        }
        else if (operador == '*')
        {
            respostaCerta = numero1 * numero2;
            UIManager.UIM.mostraAConta.text = numero1 + " x " + numero2;
        }
        else if (operador == '/')
        {
            if(numero1 < numero2 )
            {
                numero1 = numero2 + Random.Range(1, 3);
            }

            while (numero1 % numero2 != 0)
            {
                numero1 = Random.Range(0, 11);
                numero2 = Random.Range(0, 11);
            }
            respostaCerta = numero1 / numero2;
            UIManager.UIM.mostraAConta.text = numero1 + " / " + numero2;
        }
    }

    //sortear um numero de 1 a 3
    // Enviar a resposta certa para o numero sorteado e a resposta errada para outros
    private void sortearPorta()
    {
        numeroSelecionado = Random.Range(1, 4); // vai sortear de 1 a 3
        float respostaErrada1 = respostaCerta - Random.Range(1, 6); // - random.range(1, 5)
        float respostaErrada2 = respostaCerta + Random.Range(1, 6);

        if (numeroSelecionado == 1)
        {
            // Porta equerda
            UIManager.UIM.textoEsquerdo.text = respostaCerta.ToString();
            UIManager.UIM.textoDireito.text = respostaErrada1.ToString();
            UIManager.UIM.textoCentro.text = respostaErrada2.ToString();
            respawPortas();
        }
        if(numeroSelecionado == 2)
        {
            // Porta central
            UIManager.UIM.textoCentro.text = respostaCerta.ToString();
            UIManager.UIM.textoEsquerdo.text = respostaErrada1.ToString();
            UIManager.UIM.textoDireito.text = respostaErrada2.ToString();
            respawPortas();
        }
        if (numeroSelecionado == 3)
        {
            // Porta Direita
            UIManager.UIM.textoDireito.text = respostaCerta.ToString();
            UIManager.UIM.textoCentro.text = respostaErrada1.ToString();
            UIManager.UIM.textoEsquerdo.text = respostaErrada2.ToString();
            respawPortas();
        }
    }

    // função para sortear os numero1 e numero2 e chamar a as outras funções
    public void executaOperacao(char operador)
    {
        //operador = '*';
         numero1 = Random.Range(0, 11);
         numero2 = Random.Range(0, 11);
        operacao(numero1, numero2, operador);
        sortearPorta();
    }

    public void respawPortas()
    {
        if (numeroSelecionado == 1)
        {
            //esquerda
            Instantiate(portaCentral, new Vector3(0.4f, 33f, 420f), Quaternion.identity);
            Instantiate(portaDireita, new Vector3(38.7f, 33f, 420f), Quaternion.identity);
        }
        if(numeroSelecionado == 2)
        {
            Instantiate(portaEsquerda, new Vector3(-37.6f, 33f, 420f), Quaternion.identity);
            //centro
            Instantiate(portaDireita, new Vector3(38.7f, 33f, 420f), Quaternion.identity);
        }
        if(numeroSelecionado == 3)
        {
            Instantiate(portaEsquerda, new Vector3(-37.6f, 33f, 420f), Quaternion.identity);
            Instantiate(portaCentral, new Vector3(0.4f, 33f, 420f), Quaternion.identity);
            //direita
        }
    }
}
