﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortaSingle : MonoBehaviour
{
    //[SerializeField] private AudioSource _audioSource;

    public float portaSpeed = 0;

    [SerializeField]
    Rigidbody rigid;

    void Start()
    {
        //_audioSource = GetComponent<AudioSource>();
        // playAudio();
        AudioManager.AM.PlayAudioFromTheRocks(true, "Start Porta");
        rigid = GetComponent<Rigidbody>();
        rigid.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        portaSpeed = AumentaVelocidade.VE.velocidadeGeral;
    }

    // Update is called once per frame
    void Update()
    {
        rigid.AddForce(transform.forward * -portaSpeed * Time.deltaTime, ForceMode.Acceleration);
        limite();
        gameOver();
    }

    private void limite()
    {
        if (transform.position.z < -28f)
        {
            //AudioManager.AM.PlayAudioFromTheRocks(false, "Porta Passou do Limite");
            Destroy(this.gameObject);
        }
    }

    private void gameOver()
    {
        if (RespawManager.RM.gameover)
        {
            AudioManager.AM.PlayAudioFromTheRocks(false, "Game Over");
            Destroy(this.gameObject);
        }
    }

    private void playAudio()
    {
        if(this.gameObject.tag == "GrandePorta")
        {
            //_audioSource.Play();
        }
    }

    private void mover()
    {
        if (this.gameObject.tag == "GrandePorta")
        {
            transform.Translate(new Vector3(0, 0, -portaSpeed * Time.deltaTime));
        }
        else if (this.gameObject.tag == "Porta")
        {
            Debug.Log("Porta RigidBody");
            rigid.AddForce(transform.forward * -portaSpeed * Time.deltaTime, ForceMode.Acceleration);  
        }
    }
}
