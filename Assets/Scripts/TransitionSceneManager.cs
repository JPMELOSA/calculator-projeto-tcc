using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionSceneManager : MonoBehaviour
{
    #region STATIC TRANSITIONS
    public static TransitionSceneManager TSC;
    private void OnEnable()
    {
        if (TransitionSceneManager.TSC == null)
        {
            TransitionSceneManager.TSC = this;
        }
        else
        {
            if (TransitionSceneManager.TSC != this)
            {
                Destroy(this.gameObject);
            }
        }

        DontDestroyOnLoad(this.gameObject);
    }
    #endregion STATIC TRANSITIONS

    [SerializeField] private Canvas transition;
    [SerializeField] private Animator tscAnimator;

    [Header("Tempo de Espera")]
    [Range(0.5f, 3)]
    private float tempoSplashScene;
    void Start()
    {
        StartCoroutine(GoMainMenu());
    }

    public void ChangeScene(string NameScene, float time)
    {
        StartCoroutine(Countdown(NameScene, time));
    }

    IEnumerator Countdown(string NameScene, float time)
    {
        tscAnimator.SetBool("FadeIn", false);
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(NameScene);
        tscAnimator.SetBool("FadeIn", true);
    }

    IEnumerator GoMainMenu()
    {
        yield return new WaitForSeconds(2f);
        transition.gameObject.SetActive(true);
        AudioManager.AM.LoadAudioSetup();
        AudioManager.AM.PlayMusic("MAINMENU");
        ChangeScene("02-Menu", 1);
    }
}
