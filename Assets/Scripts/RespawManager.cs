﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawManager : MonoBehaviour
{
    #region STATIC RESPAW MANAGER
    public static RespawManager RM;

    private void Awake()
    {
        if (RespawManager.RM == null)
        {
            RespawManager.RM = this;
        }
        else
        {
            if (RespawManager.RM != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC RESPAW MANAGER

    [SerializeField]
    private GameObject PortaGrandePrefab;

    [SerializeField]
    private GameObject PlayerPrefab;

    [SerializeField]
    private GameObject Fireworks;

    [SerializeField]
    public bool gameover = true;   

    [SerializeField]
    public char operacao;

    public bool operacaoEscolhida = false;

    //

    void Update()
    {
        if (gameover)
        {
            colocaOperacao();
            if (operacaoEscolhida)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    inicio();
                }
            } 
        } 
    }

    public void InstaciarPortas() 
    {
        Instantiate(PortaGrandePrefab, new Vector3(-83.5f, -9.9f, 432.6f), Quaternion.identity);
        Calculadora.CA.executaOperacao(operacao);
    }

    public void inicio()
    {
        Instantiate(PlayerPrefab, new Vector3(0, -8.990016f, 14f), Quaternion.identity);
        gameover = false;
        InstaciarPortas();
    }

    private void colocaOperacao()
    {
        if (Input.GetKeyDown(KeyCode.KeypadMultiply) && operacaoEscolhida == false)
        {
            operacao = '*';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus) && operacaoEscolhida == false)
        {
            operacao = '-';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadDivide) && operacaoEscolhida == false)
        {
            operacao = '/';
            operacaoEscolhida = true;
        }
        else if (Input.GetKeyDown(KeyCode.KeypadPlus) && operacaoEscolhida == false)
        {
            operacao = '+';
            operacaoEscolhida = true;
        }
    }

    public void SetFireworks()
    {
        GameObject LeftFireWorks = Instantiate(Fireworks, new Vector3(-280, 10, 306), Quaternion.identity);
        GameObject MiddleFireWorks = Instantiate(Fireworks, new Vector3(34, 10, 306), Quaternion.identity);
        GameObject RightFireWorks = Instantiate(Fireworks, new Vector3(292, 10, 306), Quaternion.identity);

        StartCoroutine(DestroyFireworksClone(LeftFireWorks, MiddleFireWorks, RightFireWorks));
    }

    IEnumerator DestroyFireworksClone(GameObject left, GameObject middle, GameObject right)
    {
        yield return new WaitForSeconds(2f);
        Destroy(left);
        Destroy(middle);
        Destroy(right);
    }
}
