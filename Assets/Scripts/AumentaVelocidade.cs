﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AumentaVelocidade : MonoBehaviour
{
    #region STATIC VELOCIDADE
    public static AumentaVelocidade VE;

    private void Awake()
    {
        if (AumentaVelocidade.VE == null)
        {
            AumentaVelocidade.VE = this;
        }
        else
        {
            if (AumentaVelocidade.VE != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC VELOCIDADE

    public float velocidadeGeral = 500;

    [SerializeField]
    private Text mostraVelo;

    public void recebeVelocidade(float velocidade)
    {
        {
            velocidadeGeral += velocidade;
            //mostraVelo.text = "Velocidade: " + velocidadeGeral;
        }
    }
}
