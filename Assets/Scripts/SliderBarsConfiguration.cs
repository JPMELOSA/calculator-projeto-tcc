using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderBarsConfiguration : MonoBehaviour
{
    AudioManager audioManager;
    void Start()
    {
        audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    public void _EFX1Ajust(float newVolume)
    {
        if(audioManager != null)
        {
            //audioManager.EFXVolume = newVolume;
            audioManager._EFX1Ajust(newVolume);
            Debug.Log("EFX 1: " + newVolume);
        }
        else
        {
            audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        }
    }

    public void _EFX2Ajust(float newVolume)
    {
        if (audioManager != null)
        {
            //audioManager.EFXVolume2 = newVolume;
            audioManager._EFX2Ajust(newVolume);
            Debug.Log("EFX 2: " + newVolume);
        }
        else
        {
            audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        }
    }

    public void _MUSICAjust(float newVolume)
    {
        if (audioManager != null)
        {
            //audioManager.MusicVolume = newVolume;
            audioManager._MUSICAjust(newVolume);
            Debug.Log("MUSIC: " + newVolume);
        }
        else
        {
            audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        }
    }
}
