﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementButton : MonoBehaviour
{
    #region STATIC BUTTON MOVIMENT
    public static MovementButton MOV;
    private void OnEnable()
    {
        if (MovementButton.MOV == null)
        {
            MovementButton.MOV = this;
        }
        else
        {
            if (MovementButton.MOV != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC BUTTON MOVIMENT
    

    public bool leftButtonClick = false;
    public bool rightButtonClick = false;

    public void botaoEsquerdoClicado()
    {
        leftButtonClick = true;
    }

    public void botaoDireitoClicado()
    {
        rightButtonClick = true;
    }
}
