using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuSceneManager : MonoBehaviour
{
    #region STATIC MENU SCENE MANAGER
    public static MenuSceneManager MSM;

    private void Awake()
    {
        if (MenuSceneManager.MSM == null)
        {
            MenuSceneManager.MSM = this;
        }
        else
        {
            if (MenuSceneManager.MSM != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion STATIC MENU SCENE MANAGER

    [Header("Buttons")]
    [SerializeField] private GameObject btnIniciar;

    [SerializeField] private GameObject Buttons;

    [SerializeField] private GameObject btnMenuPrincipalPause;

    [Header("Animators")]

    [SerializeField] private Animator adicaoAnimator;

    [SerializeField] private Animator subtracaoAnimator;

    [SerializeField] private Animator multiplicacaoAnimator;

    [SerializeField] private Animator divisaoAnimator;

    [SerializeField] private Animator menuPrincipalAnimator;

    [SerializeField] private Animator Score;

    // Pause
    [Header("Pause")]
    public bool isPause;

    [SerializeField] private GameObject equationCover;

    [SerializeField] private GameObject pause;

    [SerializeField] private GameObject play;

    [SerializeField] public GameObject pausePlay;

    [Header("Camera")]

    [SerializeField] private Animator CameraAnimator;

    void Start()
    {
        abrirButtons(true);
        
    }

    public void _Pause()
    {
        AudioManager.AM.PlaySFX("PAUSE");
        pause.SetActive(isPause);
        play.SetActive(!isPause);
        if (isPause)
        {
            Time.timeScale = 1;
            AudioManager.AM.PlayAudioFromTheRocks(true, "Menu Manager PAUSE = false");
            isPause = false;
            btnMenuPrincipalPause.SetActive(false);
        }
        else
        {
            Time.timeScale = 0;
            AudioManager.AM.PlayAudioFromTheRocks(false, "Menu Manager PAUSE = true");
            isPause = true;
            btnMenuPrincipalPause.SetActive(true);
        }
        cover(isPause);
    }

    private void cover(bool cover)
    {
        equationCover.SetActive(cover);
    }

    #region Buttons
    public void abrirButtons(bool abrir)
    {
        adicaoAnimator.SetBool("PlayAdicao", abrir);
        subtracaoAnimator.SetBool("PlaySubtracao", abrir);
        multiplicacaoAnimator.SetBool("PlayMultiplicacao", abrir);
        divisaoAnimator.SetBool("PlayDivisao", abrir);
        menuPrincipalAnimator.SetBool("AbrirMenu", abrir);
    }

    public void _adicao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        abrirButtons(false);
        RespawManager.RM.operacao = '+';
        RespawManager.RM.operacaoEscolhida = true;
        StartCoroutine(desativaButtons());
    }

    public void _divisao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        abrirButtons(false);
        RespawManager.RM.operacao = '/';
        RespawManager.RM.operacaoEscolhida = true;
        StartCoroutine(desativaButtons());
    }

    public void _multiplicacao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        abrirButtons(false);
        RespawManager.RM.operacao = '*';
        RespawManager.RM.operacaoEscolhida = true;
        StartCoroutine(desativaButtons());
    }

    public void _subtracao()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        abrirButtons(false);
        RespawManager.RM.operacao = '-';
        RespawManager.RM.operacaoEscolhida = true;
        StartCoroutine(desativaButtons());
    }

    public void _botaoIniciar()
    {
        AudioManager.AM.PlaySFX("MOUSECLICK");
        btnIniciar.SetActive(false);
        RespawManager.RM.inicio();
        Score.SetBool("Play", true);
        pausePlay.SetActive(true);
        // --------------------------------------------- Start Game = True
        CameraAnimator.SetBool("StartGame", true);
    }

    public void _retornarMenuPrincipal()
    {
        if (isPause)
        {
            Time.timeScale = 1;
        }
        AudioManager.AM.PlaySFX("MOUSECLICK");
        TransitionSceneManager.TSC.ChangeScene("02-Menu", 1);
    }

    IEnumerator desativaButtons()
    {
        yield return new WaitForSeconds(1);
        Buttons.SetActive(false);
        btnIniciar.SetActive(true);
    }

    public void ButtonsAfterGameOver()
    {
        Buttons.SetActive(true);
        abrirButtons(true);
        Score.SetBool("Play", false);
        // --------------------------------------------- Start Game = false
        CameraAnimator.SetBool("StartGame", false);
    }

    #endregion Buttons
}
